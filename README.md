# RÉSOLU : Réseaux éthiques et solutions ouvertes pour libérer vos usages

Site web des fiches RÉSOLU, fiches logiciels libres pour les associations.

## Contribuer

Le site web est généré grâce à [MkDocs](https://www.mkdocs.org/), un générateur de sites statiques orienté documentation, développé en python.

Les fiches sont rangées dans le dossier `docs`. Elles sont éditées en markdown.

La navigation est définie dans le fichier `mkdocs.yml`, dans la section `nav:`.

### Générer en local

Pour générer le site en local, installer Mkdocs et ses dépendances.

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements
mkdocs serve
```

### Bonnes pratiques

Il est conseillé de créer des "branches" pour plus de lisibilité dans les contributions.

### Mettre à jour le site

La mise à jour s'effectue automatiquement lorsque la branche `master` est mise à jour.
