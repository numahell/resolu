---
title: Collaborer
---

Per lavorare in gruppo, è necessario utilizzare strumenti digitali adeguati
{: .slogan-subtitle }

La collaborazione è essenziale all'interno di qualsiasi struttura. Esistono molti strumenti digitali per lavorare insieme. Vediamo  qualche riflessione da considerare prima di decidere quali strumenti utilizzare.

## Collaborare è partecipare a un lavoro con altre persone

Attivare i propri collaboratori e collaboratrici in modo efficace per ottenere un lavoro collettivo è auspicabile in molti casi, ma non sempre è così facile come vorremmo... I diversi orari, la distanza fisica, i vincoli  nella circolazione delle informazioni e molte altre ragioni possono rendere complicata la collaborazione.  Fortunatamente, esistono soluzioni per (provare a) risolvere questi problemi! Tuttavia, non  sono tutte adatte agli utilizzi e alle abitudini e alcuni strumenti che sembrano pratici all'inizio possono diventare limitanti. Ecco perché è importante padroneggiare gli strumenti: non solo sapere come usarli, ma avere il controllo su di essi, in modo da non dipendere da altri che potrebbero decidere unilateralmente sullo sviluppo o sulla chiusura di uno strumento.


Le schede di **Collaborare** presentano nel dettaglio alcune soluzioni che permettono di rispondere ai bisogni della collaborazione e delle quali è possibile **mantenere il controllo**.
{: .encart }

## Una questione di fiducia

Utilizzare un servizio online (diciamo anche un servizio cloud), significa salvare le proprie informazioni **sul computer di qualcun altro**. È quindi necessario fidarsi di questo altro e conoscerne le motivazioni. Ad esempio, se ha  interesse a farti adottare un certo modo di lavorare, può  modificare i suoi strumenti per spingerti in quella direzione.

Allora sei costretto a lavorare da solo, salvando i tuoi file solo  in locale sul tuo computer? Per fortuna no: puoi archiviare i  tuoi file utilizzando i servizi di qualcuno  di cui ti fidi.


Ad esempio, alcune associazioni o società si impegnano a sottoscrivere un codice di condotta e offrono soluzioni basate su Nextcloud, una suite di strumenti che consente sia l'archiviazione che la condivisione di documenti, l'editing collaborativo o la gestione del calendario [Nextcloud](nextcloud.md). Insomma, devi trovare qualcuno di tua fiducia e valutarne l'affidabilità, oppure  devi crearti il tuo servizio online.
{: .encart }

## Condividere sì, ma farlo bene

Quando si **condividono** dei documenti, è importante che tutti i collaboratori possano visualizzarli e modificarli nelle stesse condizioni. I formati aperti sono distribuiti con licenza libera, possono essere aperti e modificati con la maggior parte dei software 
(liberi o non liberi). Questa caratteristica  si chiama **interoperabilità**. Questa viene anche descritta in un documento che presenta una serie di norme e di buone pratiche comuni alle amministrazioni pubbliche francesi nell’ambito informatico: il riferimento generale di interoperabilità (RGI).

Il formato OpenDocument è un esempio di formato aperto per tutte le applicazioni per ufficio (.odt per il testo, .ods per il foglio di calcolo…)

## Riunioni e appuntamenti


Strumenti come Skype o Doodle sono ampiamente utilizzati per l'organizzazione di riunioni a distanza. Tuttavia, questi servizi ci rendono dipendenti da imprese i cui interessi non sono necessariamente i nostri. Skype, ad esempio, è diventato molto meno fluido nella sua versione gratuita da quando è stato acquisito da Microsoft, e Doodle ha deciso da un giorno all’altro di  far pagare una serie di funzioni. Per evitare questo, [Jitsi Meet](../Comunicare/jitsi-meet.md) e Framadate, due software liberi, possono facilmente sostituire queste soluzioni. Questi sono due esempi che mostrano che per lavorare insieme non serve obbligare i collaboratori a diventare prigionieri di un servizio.


[![guide-libre-association](../img/guide_libre_association.jpg){: .illustration }](https://framabook.org/guide-libre-association/){target=_blank}
*[April](https://www.april.org/){target=_blank} / [Framasoft](https://framasoft.org/fr/){target=_blank}  
[Guida alla Associazione Libera](https://framabook.org/guide-libre-association/){target=_blank} -
[[PDF ↓]](http://guide.libreassociation.info/includes/guide-libre-association-framabook-version-30-decembre-2015-couleur-ecran.pdf){target=_blank} -
[[EPUB ↓]](http://guide.libreassociation.info/includes/guide-libre-association-framabook-version-30-decembre-2015.epub){target=_blank}         
Degli applicativi per liberare il vostro progetto associativo.   
Framabook 2016  
Licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/){target=_blank}*
{: .figure }
