---
title: Gratuità
---

Il software libero è gratuito, come è possibile?
Lo è sempre?
{: .slogan-subtitle }

## Alcuni programmi sono già stati pagati

**I programmi liberi sono gratuiti perché sono già stati pagati**. Dedicando del tempo e del denaro al loro sviluppo. Ecco come software come LibreOffice (suite per ufficio) o GIMP (editing e fotoritocco di immagini) permettono di fare più o meno le stesse cose delle loro alternative a pagamento. Progetti come Wikipedia o Framasoft vivono solo grazie alle donazioni di utilizzatori che non comprano prodotti, ma partecipano ad un'economia alternativa per mantenere dei [beni comuni digitali](../Organizzare/comuni.md).

## Alcuni servizi online

Alcuni software sono servizi online. Permettono di accedere agli strumenti con un browser senza la necessità di installare il software sul proprio computer. Per rendere disponibili tali servizi, chi li ospita ha bisogno di server e di tempo per occuparsene. Mentre alcuni strumenti sono poco costosi da installare, altri possono richiedere più risorse. Per esempio, la fornitura di spazio di archiviazione richiede di avere abbastanza dischi rigidi per memorizzare tutto e garantire i backup, o anche l'esternalizzazione dell'infrastruttura tecnica a un fornitore di server.  

I fornitori professionali di hosting, anche se offrono servizi basati su software open source, devono almeno far pagare l'hosting per coprire i costi di infrastruttura e gli stipendi. Allo stesso modo, i fornitori associativi, che si avvalgano o meno di professionisti, possono chiedere un contributo finanziario o incoraggiare le donazioni. Non è il software libero che viene pagato, ma il servizio.
{: .encart }

## Ma Google è gratuito?

Eppure Google è una delle aziende più ricche del mondo. Quando si utilizzano i servizi gratuiti di una tale società, non si paga in denaro ma in attenzione e dati personali. I veri clienti del GAFAM (Google, Apple, Facebook, Amazon, Microsoft) non sono i servizi gratuiti, ma gli inserzionisti che vendono spazi pubblicitari o i mediatori di dati che, grazie alle loro tecniche di data mining, realizzano enormi profitti rivendendo profili ad altri clienti.

La raccolta, il recupero e l'uso dei dati personali solleva gravi problemi di privacy e contribuisce a un'economia in cui pochi attori esercitano un grande potere. Grazie ai loro servizi, che non vengono pagati con denaro, alcune aziende sono diventate quasi indispensabili nella nostra vita quotidiana. Noi inconsapevolmente diamo loro un enorme potere su molti aspetti della nostra vita pubblica e privata.
{: .encart }

## Perché pagare per i servizi?

L'hosting di servizi online ha un costo. Questi servizi funzionano sui computer di chi li ospita, che deve coprire i costi dell'hardware (server, dischi rigidi, ecc.), della connessione a Internet, ma anche la manodopera per la fornitura e la manutenzione, la sicurezza dei dati, ecc.

Pagare un fornitore di servizi di hosting etico significa contribuire a un'economia solidale, basata sullo scambio e la condivisione di competenze. 

Passare attraverso una struttura più umana non solo permette di creare legami sociali, ma anche di agire direttamente sugli strumenti che utilizziamo per farli evolvere secondo i nostri bisogni. Quindi non è raro che un'azienda o un'associazione specializzata in software libero contribuisca anche allo sviluppo del software che utilizza. Il ciclo tende ad essere virtuoso.

Ci sono molti fornitori di hosting etici. Ad esempio, questi fornitori si impegnano, attraverso un manifesto e una carta comune, a rispettare i prezzi di utilizzo e ad offrire in modo trasparente servizi liberi ad un prezzo ragionevole e adeguato ai costi di implementazione. Potete trovare il servizio di hosting che fa per voi su [chatons.org](http://chatons.org/)!
{: .encart }

## +/-

Pagare o non pagare è una scelta che si misura in termini di fiducia che si ha nell'altra parte. Una soluzione proprietaria gratuita può diventare una soluzione a pagamento da un giorno all'altro, e l'utente si trova costretta/o a accettare scelte che all'inizio non voleva fare.

Pagare un servizio gratuito può sembrare strano: beh, sapete per cosa pagate e perché lo pagate!
{: .slogan-contenu }

Un esempio è il servizio Doodle della società svizzera Doodle AG. Questa società ha fornito gratuitamente un famoso strumento di indagine e diario collaborativo e ha venduto spazi pubblicitari. Nel 2019, ha improvvisamente deciso di rendere a pagamento questo servizio con una versione di prova molto limitata. Gli utenti che hanno utilizzato questo servizio per anni hanno dovuto fare una scelta forzata. <br>
L'associazione Framasoft propone da tempo un'alternativa molto efficiente chiamata Framadate...
{: .encart }
