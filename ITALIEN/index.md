---
title: RESOLU
---

# Reti Etiche e Soluzioni Aperte per Liberare i vostri Utilizzi.

![resolu](./img/cover_small.jpg){: #cover srcset="/img/cover_small.jpg 480w, /img/cover_big.jpg 800w, /img/cover.jpg 2x" sizes="(max-width: 480px) 440px, 800px"}](https://framabook.org/resolu/){target=_blank  rel=noopener}

Costruire un mondo migliore è possibile solo con gli strumenti che ne danno la libertà
{: .slogan-subtitle }

Agire per l'economia sociale e solidale richiede molta organizzazione, comunicazione e collaborazione. Promuovere un modello di produzione alternativo basato sulla solidarietà e sulla condivisione è uno sforzo collettivo che non può fare a meno degli strumenti digitali per essere efficace. Spesso questi strumenti vengono scelti per impostazione predefinita e non corrispondono alle esigenze o ai valori delle organizzazioni che li utilizzano. 

L'obiettivo di \[Resolu\] è accompagnare la tua organizzazione verso l'adozione di soluzioni alternative libere. Alcuni strumenti ti vengono presentati sotto forma di schede; sono stati selezionati per la loro rilevanza per le pratiche digitali dei collettivi. Per comprendere meglio le sfide di questa transizione per la tua organizzazione, troverai anche schede informative che evidenziano la coerenza tra i valori del software libero e quelli dell'economia sociale e solidale. Speriamo che la lettura di questa guida ti convinca  della scelta di mantenere il controllo dei tuoi utilizzi digitali scegliendo il software libero. Cerchiamo di essere risoluti [resolu] nel sostenere insieme un modello di società basato sull'accessibilità, la contribuzione, l'apertura e la solidarietà.

## Mode d'emploi

Le versioni PDF e le fonti grafiche e *Open Document Format* di questo manuale sono disponibili per tutti su Framabook. [Framabook](https://framabook.org/resolu/).
{: .encart }

Sotto la licenza gratuita menzionata qui sotto, potete usare tutti questi contenuti, compreso questo sito. Poiché le schede che compongono questo manuale sono indipendenti l'una dall'altra, potete stamparle o modificarle separatamente e distribuirle come volete. Non dimenticate di menzionare la licenza e gli autori, anche in caso di modifica quando si aggiungono altri autori alla lista.

## Versione 1.0

### Realizzazione (2020)

[![framasoft_logo](./img/framasoft_logo.png)](https://framasoft.org/)
[![picasoft_logo](./img/picasoft_logo.png)](https://picasoft.net/)
[![cemea_logo](./img/cemea_logo.jpg)](http://www.cemea.asso.fr/)
{: .home-realisation-logos}

<br>
Con il sostegno di:

[![fondation_free_logo](./img/fondation_free_logo.png)](https://www.fondation-free.fr/)
{: .home-realisation-logos}

### Coautori, in ordine alfabetico per cognome

Audrey Guélou, Christophe Masutti, Pascal Gascoin, Rémi Uro, Stéphane
Crozat

### Licenza

[![Creative Commons – Attribuzione – Condividi allo stesso modo (CC-By-Sa)](./img/by-sa.png)](http://creativecommons.org/licenses/by-sa/3.0/)

Questo manuale è reso disponibile secondo i termini della [Creative Commons – Attribuzione – Condividi allo stesso modo (CC-By-Sa)](http://creativecommons.org/licenses/by-sa/3.0/).

### Progetto grafico

[Odigi.eu](http://www.odigi.eu/)

Font utilizzate : Liberation, TeX Gyre Adventor

### Crediti delle illustrazioni

Copertina : [Shane Rounce](https://unsplash.com/@shanerounce)
sur
[Unsplash](https://unsplash.com/s/photos/team)

Sezione « Zourit » : [Stephanie Klepacki](https://unsplash.com/@sklepacki)
sur
[Unsplash](https://unsplash.com/s/photos/octopus)

Sezione « Confidentialité » :
[Antonella Brugnola](https://unsplash.com/@ziaantonella)
sur
[Unsplash](https://unsplash.com/s/photos/privacy)

Sezione « Organiser » : [Jason Leung](https://unsplash.com/@ninjason)
sur
[Unsplash](https://unsplash.com/s/photos/organize)

Sezione « Conduite du changement » : [Javier Allegue Barros](https://unsplash.com/@soymeraki)
sur
[Unsplash](https://unsplash.com/s/photos/sign-direction)

Sezione « Communs numériques » : [Bekir Dönmez](https://unsplash.com/@bekirdonmeez)
sur
[Unsplash](https://unsplash.com/s/photos/team)

---

[https://framabook.org](https://framabook.org/)

---

Traduzione italiana: 

Roberto Marcolin (Nilocram)

Ilario Quinson

Roberto Resoli

Danilo Spada

Daniele Zambelli

LinuxTrent - https://www.linuxtrent.it
