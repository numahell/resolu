---
title: Communiquer
---

Come facilitare la comunicazione interna o esterna?
{: .slogan-subtitle }

n qualsiasi organizzazione, le persone devono comunicare tra loro e con il pubblico. Ci sono molti strumenti digitali per comunicare, ma non tutti sono adatti ad ogni situazione. 

È importante conoscere le diverse soluzioni per poter scegliere il più appropriato.

Che sia **internamente** con i membri della sua struttura o esternamente con il pubblico o con altre organizzazioni, è necessario sapere come cambiare strumento secondo gli obiettivi. Gli strumenti numerici hanno rivoluzionato le nostre pratiche per più di 30 anni. Per organizzarsi, collaborare, farsi conoscere, ecc. la conoscenza di questi strumenti è fondamentale. Ma gli strumenti non sono neutrali e sceglierli in modo errato può compromettere la riservatezza dei dati scambiati, costringere le persone a mettere a rischio la propria privacy o consumare molte più risorse di quanto necessario.

Facebook, ad esempio, è uno strumento efficace quando si tratta di comunicare con un pubblico la cui presenza su questo social può essere valutata. Tuttavia, se è l'unico canale di comunicazione esterno è necessario che coloro che non utilizzano questo servizio si registrino. In questo modo, invece di pubblicare sempli­cemente informazioni e link, un effetto di rete si rivelerà un effetto di rete. Questo renderà dipendenti dagli strumenti di Facebook anche i vostri collaboratrici/tori e il vostro pubblico.
{: .encart }

Le bolle artificiali impediscono di raggiungere un nuovo pubblico. Offuscano i confini che i vostri collaboratori devono mantenere tra il loro lavoro e la loro vita privata. <br>
E questo è solo un assaggio di una dipendenza orchestrata contro la tua volontà.
{: .slogan-contenu }

Questa serie di schede informative offre alcuni strumenti pratici ed etici per aiutarvi a comunicare meglio.
{: .text-center }

## La comunicazione esterna

Essere visibili su Internet è molto importante per raggiungere il maggior numero possibile di persone.

Tre strumenti possono essere utili per aumentare la vostra visibilità: un sito web, i social media e le e-mail. Ma le buone pratiche relative a questi strumenti non sono sempre ovvie. 

Le schede dedicate alla comunica­zione esterna evidenzieranno le buone pratiche con questi strumenti, soprattutto in termini di rispetto della privacy pubblica, e di coerenza in relazione ai temi trattati.

## La communication interne

Non tutti gli strumenti di comunicazione sono adatti per lavorare in squadra. Alcuni sono progettati per discussioni personali tra amici, altri per lunghe corrispondenze su un argomento, altri per discussioni di gruppo istantanee. 

Noi affronteremo anche l'argomento privacy: quando si comunica tra persone che collaborano tra loro volontarie/i o professioniste/i, è frequente lo scambio di informazioni che dovrebbero essere tenute segrete. È quindi tanto più più importante utilizzare uno strumento affidabile e assicurarsi di prestare attenzione ai destinatari dei messaggi "sensibili". 

Un primo modo per risolvere questo problema è quello di adottare strumenti che permettono di  comunicare per gruppi di lavoro.

[Mattermost](mattermost.md), ad esempio, permette la comunica­zione di team, in privato o in pubblico, e poiché si tratta di software libero, il codice è acces­sibile e controllato pubblicamente, in modo che diventi presto evidente se il programma conte­nesse delle scappatoie che permettano a terzi indesiderati di carpire i messaggi.
{: .encart }

[![stand-mozilla](../img/stand_mozilla.jpg){: .illustration }](https://commons.wikimedia.org/wiki/Category:Lyon_JDLL_2018#/media/File:2018-03_JDLL_Stande_Wikimedia_France_01.jpg){target=_blank}
*Molti eventi dedicati al software libero sono organizzati ogni anno in tutto il mondo. Non esitate ad andarci!   
Stand Wikimedia France (gestito da volontari dei progetti Wikimedia) alle Giornate del Software Libero 2018 a Lione, Francia.
Fonte: [Wikimedia](https://commons.wikimedia.org/wiki/Category:Lyon_JDLL_2018#/media/File:2018-03_JDLL_Stande_Wikimedia_France_01.jpg){target=_blank}  
Photo : Antoine Lamielle      
Licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/){target=_blank}*
{: .figure }
