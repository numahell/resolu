---
title: Gestisci la tua contabilità con Garradin
---

Strumento di gestione della contabilità per le associazioni di  piccole e medie dimensioni
{: .slogan-subtitle }

Gestire le casse della propria associazione a volte richiede molto tempo ed energia. Sia che stiate affogando in una moltitudine di fogli di calcolo, che stiate cercando strumenti e metodi, o che abbiate un servizio troppo costoso e proprietario, una soluzione alternativa è possibile per adempiere in modo efficiente ai vostri obblighi contabili. 

Garradin è uno strumento completo e facile da usare. Non c'è bisogno di essere un contabile per usarlo; è stato progettato per facilitare i compiti di contabilità associativa. Grazie alle sue numerose funzionalità e alla sua attiva comunità di utilizzatori, Garradin soddisfa le esigenze delle piccole e medie strutture. Provatelo e vedrete voi stessi; è così che ognuno troverà il suo conto! Garradin è uno strumento completo e facile da usare. Non c'è bisogno di essere un contabile per usarlo; è stato progettato per facilitare i compiti di contabilità associativa. Grazie alle sue numerose funzionalità e alla sua attiva comunità di utilizzatori, Garradin soddisfa le esigenze delle piccole e medie strutture. Provatelo e vedrete voi stessi; è così che ognuno troverà il suo conto!

### Tutta la gestione della contabilità in un unico pacchetto software

Il primo merito di Garradin è quello di rendere la contabilità accessibile a tutti, pur consentendo una gestione completa, in grado di soddisfare un vero contabile. Abbiamo uno strumento di contabilità intuitivo che, ad esempio, rende accessibili la contabilità a partita doppia e il piano contabile associativo.

Garradin comprende la produzione dei risultati contabili e dei bilanci che sono obbligatori per le assemblee generali e le richieste di sovvenzione. Infine, le rappresentazioni grafiche riportano a colpo d'occhio il budget dell'associazione per il monitoraggio quotidiano.

Un altro vantaggio è il consolidamento di tutti i dati contabili e dei soci (comprese le quote associative) in un unico luogo, garantendo così la sicurezza dei dati e degli archivi senza il rischio di confusione con le versioni obsolete.

### Volete di più?

Risparmiate tempo permettendo a ciascun membro di registrarsi e di aggiornare i propri dati personali. Hai bisogno di recuperare tutti questi dati? L'esportazione in CSV rende tutto più semplice - soprattutto se si sfrutta il numero illimitato di iscrizioni.

Incoraggiate l'iniziativa e l'intelligenza collettiva per capitalizzare tutte le informazioni utili in un wiki interno collaborativo. Per dare più visibilità alle vostre azioni, scegliete le pagine che appariranno sul vostro sito web pubblico! Si mantiene il controllo dei diritti di accesso e di modifica.

Infine, poiché Garradin è un software libero, potete (chiedete a qualcuno di farlo) sviluppare un'estensione per soddisfare le esigenze specifiche della vostra associazione. Se pensate che possa essere utile ad altre organizzazioni, potete aggiungerla alle estensioni già condivise dalla comunità di Garradin!

### Contabilizzare in libertà


I dati contabili della vostra associazione e i dati personali dei vostri soci sono dati critici. È meglio poter scegliere come questi dati vengono memorizzati, elaborati, utilizzati e diffusi.

L'utilizzo di un software proprietario per la gestione contabile della vostra associazione implica la fiducia dell'organizzazione che pubblica questo software per quanto riguarda queste modalità. Poiché il codice non è accessibile, è impossibile sapere come vengono gestiti i dati affidati. 

Al contrario, l'utilizzo di un software libero come Garradin garantisce un maggior grado di trasparenza:

- Se scegliete di installare Garradin sul vostro server, la trasparenza è totale: il codice che elabora i vostri dati è aperto e i vostri dati rimangono sul vostro server.
- Se scegliete di utilizzare Garradin senza installarlo, il codice è sempre aperto e vi fidate dell'organizzazione che ospita i vostri dati.

Qualunque sia l'opzione scelta, si rimane indipendenti: la possibilità di importare ed esportare i dati in un formato libero (CSV) consente di migrare su un'altra installazione Garradin o di utilizzare altri software.

La gestione collaborativa di questi dati e del wiki porta un'altra dimensione etica: quella di una governance più orizzontale e trasparente della vostra associazione. Una buona gestione dei diritti di accesso consente sempre di scegliere le modalità di questa collaborazione.

La possibilità data ai membri di consultare direttamente i dati contabili su Garradin ha anche un impatto ecologico: si evita l'invio multiplo di e-mail contenenti file non direttamente accessibili ai membri; messaggi pesanti e avidi di risorse energetiche.

Ed infine, Garradin è un software libero, mantenuto e migliorato grazie alla contributi dei suoi utenti. Se desiderate sostenere questo modello, è possibile fare una donazione all'associazione Kidideux, che ospita Garradin e ne assicura la continuità.


### Tre modi possibili per iniziare oggi


- Prova Garradin senza installare nulla creando un account per la tua associazione sul sito ufficiale ([garradin.eu](http://garradin.eu/)).
- Installare Garradin sul proprio server (seguendo le istruzioni presenti nella documentazione)
- Utilizzando il vostro Garradin, incluso nella suite di software [Zourit](../collaborer/zourit.md).

Una comunità è attiva per accompagnarvi: più di 2000 associazioni utilizzano Garradin, e sono disponibili diversi spazi per discussioni e documenti.
