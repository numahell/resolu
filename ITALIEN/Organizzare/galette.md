---
title: Gestire la vostra associazione con Galette
---

Uno strumento di gestione degli iscritti e del pagamento di quote online
{: .slogan-subtitle }

Per rispondere ai bisogni delle piccole associazioni, Galette (*Gestionnaire d’Adhérents en Ligne Extrêmement Tarabiscoté mais Tellement Efficace* – ovvero *Gestore delle iscrizioni online estremamente sovraccaricato ma così efficiente*) risponde ai bisogni concreti e si basa sulla esperienza degli utilizzatori. Questa applicazione funziona come servizio online e permette una gestione collettiva: tessere associative, mailing, calendari, abbonamenti.


## Installazione facile

Potete installare Galette su di un computer locale (ricordate di fare backup frequenti), ma l’interesse di Galette è soprattutto di lavorare online per favorire il lavoro collaborativo.

Va da sé che la facilità è sempre relativa alle competenze disponibili. Tuttavia se nella vostra associazione avete un socio che ha già un proprio blog o che ha già installato un servizio su di un server remoto, l’installazione di Galette non dovrebbe dare problemi. Rispetto al guadagno di tempo e alla facilità di gestione che ci si aspetta, può essere interessante per la vostra associazione trovare una persona competente che potrà farsi carico (nel tempo!) dell’istanza Galette dedicata alla vostra organizzazione.


**Prima –** La gestione delle iscrizioni era assicurata dalla funzione di tesoreria dell’associazione, che utilizzava una tabella su di un computer personale. I solleciti, gli aggiornamenti erano tempo di volontariato sottratto ad altri progetti. Le offerte, gli abbonamenti e le ricevute erano contabilizzati e trattati uno a uno.<br>
**Oggi –** Con Galette la gestione può essere trattata in maniera più collettiva. Le mail di sollecito sono inviate automaticamente, è possibile ottenere uno stato di tesoreria in qualche click, come  pure le schede dei soci. La gestione non è più svolta da una sola persona confinata a questo ruolo.
{: .encart }

## Vantaggi

Il primo vantaggio di Galette è appunto la gestione in proprio, che permette ai membri del direttivo di una associazione di lavorare insieme alla sua gestione. Ciò permette a tutti i soci di connettersi, pagare la propria quota, aggiornare le proprie informazioni personali, interagire grazie a un servizio di messaggistica, di prestito (libri o oggetti vari), di gestione degli eventi (sono disponibili diverse estensioni, più o meno specializzate).

## Un’applicazione concepita da utilizzatori per utilizzatori

Galette è un esempio chiaro di cosa sia un’applicazione libera. Inizialmente è stata sviluppata da un’associazione di Lyon di utenti d software libero che non trovava soluzioni per gestire le iscrizioni. Lo sviluppo di Galette è poi continuato proponendo l’applicazione a tutte le associazioni che ne sentivano il bisogno e valorizzando le loro stesse valutazioni al fine di migliorarla.

Poiché il codice non solo è disponibile ma anche collaudato, Galette può essere utilizzato con grande fiducia.
{: .slogan-contenu }

Se utilizzate Galette fatelo sapere agli sviluppatori (sul [sito ufficiale](https://galette.eu/site/fr/) e contribuite voi stessi anche solo dando un feedback.


