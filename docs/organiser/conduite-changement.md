---
title: Conduite du changement
---

Adopter de nouveaux outils, changer les habitudes : une ambition à hauts
risques
{: .slogan-subtitle }

## Éviter l'échec

Ne tournons pas autour du pot : imposer de nouveaux outils (libres ou
non) à vos collaborateur⋅rice⋅s, c'est prendre des risques ! Sans
discussion préalable et sans préparation, l'échec pointera son nez. La
stratégie à adopter, dans ce domaine, consistera à pratiquer
l'ouverture, la transparence, et à permettre une prise de décision la
plus horizontale possible. Avec des logiciels libres, même si vous êtes
persuadé·e du bien-fondé de votre décision, l'absence de consensus est
fatale. Les logiciels libres ne sont pas que des communs disponibles
pour tou·te·s : les utiliser collectivement, c'est aussi prendre
conscience qu'on participe à ces
communs numériques, et à ce titre, l'effort doit lui aussi être partagé
et non imposé.

## Ménager les tensions

De nombreux facteurs d'inhibition, plus ou moins justifiés, empêchent
une personne d'adopter spontanément de nouvelles pratiques. Face aux
logiciels libres on a tendance à penser qu'un logiciel privateur et
payant sera bien plus efficace pour la structure et rendra plus facile
la prise en main individuelle. Parfois, les barrières psychologiques
sont bien plus difficiles à franchir que des contraintes techniques
vérifiables.

Toutes ces tensions dans l'esprit des utilisateur⋅rice⋅s doivent être
traitées avec la plus grande bienveillance.
{: .slogan-contenu }

Il ne faut pour autant pas nier qu'un logiciel libre manque parfois
d'ergonomie ou propose une interface difficile à appréhender. Il n'est
pas toujours aisé de convaincre ses collaborateur⋅rice⋅s que les valeurs
de libertés, de partage et de solidarité portées par les logiciels
libres valent quelques menus sacrifices sur l'autel de l'esthétique.
De même, adopter des logiciels libres, c'est souvent utiliser des
protocoles, des formats et des concepts inconnus jusqu'alors par les
utilisateur⋅rice⋅s : cela suppose de nouveaux apprentissages avec des
niveaux de compréhension différents. C'est pourquoi il est important
non seulement d'évaluer l'adhésion de vos collaborateur·rice·s mais il
est tout aussi important de discuter avec eux⋅elles d'une période de
transition à la fois technique et organisationnelle.

N'imposez jamais un logiciel libre : on n'assène jamais la liberté à
autrui !
{: .slogan-contenu }

## Les contournements

Il est important, avant tout changement, de bien analyser les pratiques
individuelles. Un questionnaire ne suffira pas car toutes les pratiques
ne peuvent pas être formalisées avec des mots. Il faut se réunir,
échanger de manière ouverte et même envisager d'observer longuement les
futur·e·s utilisateur·rice·s de logiciels libres.

Par exemple, dans un flux de production, les utilisateur⋅rice⋅s peuvent
avoir des habitudes qui ne correspondent pas à l'usage normal du
logiciel qu'il⋅elle⋅s utilisent. Par manque d'expertise ou de
formation, ou parce que le logiciel ne permet pas certaines manœuvres,
les personnes utilisatrices peuvent mettre en œuvre des stratégies pour
contourner les contraintes du logiciel et obtenir un résultat. Comment
ferez-vous pour interpréter ces manœuvres et les intégrer au mieux dans
le nouveau système que vous allez mettre en place à base de logiciel
libre ?
{: .encart }

### L'interopérabilité, c'est bien, mais cela suppose de nouvelles pratiques

Nous avons mentionné les **formats**. Un format ouvert
ou libre est dit interopérable parce que ses spécifications techniques
étant connues de tou⋅te⋅s, son interprétation est possible
d'innombrables manières.

C'est pour cette raison que, par exemple, le format OpenDocument (comme
le .odt qu'on peut créer avec la suite bureautique LibreOffice) est
encouragé par de nombreuses administrations publiques et il est même
défini par des normes (dont l'ISO 26300), ce qui n'est pas le cas pour
des formats fermés (comme le .doc de Microsoft).

Utiliser ces formats dans un environnement où les logiciels
propriétaires imposent leurs propres usages et formats, cela implique de
devoir s'adapter.<br><br>
Par exemple, si une administration publique envoie à votre association
un questionnaire au format .docx (contrairement aux préconisations), il
sera possible de le lire et l'enregistrer avec LibreOffice, mais cela
implique une certaine attention lors de la conversion. Un problème qui
ne se posait pas auparavant.
{: .encart }

## Êtes-vous légitime ?

Vous êtes passionné⋅e de logiciels libres, vous utilisez personnellement
un ordinateur sous GNU/Linux, on vous demande toujours des conseils et
vous dépannez vos collègues et vos ami⋅e⋅s pour qui vous être prêt⋅e à
dépenser beaucoup de temps. Vous êtes formidable !

Mais cela fait-il de vous pour autant la meilleure personne pour
planifier la transition vers le Libre de toute une organisation, comme
une association avec ses membres bénévoles et ses salarié⋅e⋅s ?
Peut-être que oui : à condition d'élaborer un plan communément accepté
et surtout de **ne pas être le⋅la seul⋅e à décider**.

-   **le « bus factor »** (mesure du risque dû à
    l'absence de partage d'informations) : la personne spécialiste qui
    a installé les logiciels libres sur toutes les machines du
    secrétariat vient d'être malheureusement reversé par un bus et ne
    sera plus là pour les prochaines mises à jour. Et voilà des
    collaborateur⋅rice⋅s réduit⋅e⋅s à travailler avec des versions
    obsolètes de logiciels parce qu'il n'y a plus personne pour s'en
    occuper.

-   **L'exclusion** : une majeure partie des membres bénévoles d'une
    association décide d'adopter [Mattermost](../communiquer/mattermost.md) pour
    [communiquer entre eux⋅elles](../communiquer/index.md) sans se
    soucier des autres membres qui ne trouvent pas le temps de
    s'auto-former au logiciel ou de l'installer : le procédé devient
    exclusif et les membres isolé⋅e⋅s quittent l'association.

## Évaluer les coûts

Tout changement implique des coûts. Ils peuvent être humains ou
financiers et s'évaluent dans le temps. Si vous décidez d'utiliser des
services éthiques en ligne, vous allez devoir soit acheter un serveur ou
un hébergement de serveur, soit louer l'hébergement du service auprès
d'une instance maintenue par un tiers de confiance (comme l'un des
CHATONS).

Auparavant votre association pouvait profiter de l'offre gratuite de
stockage chez DropBox. Désormais non seulement vous allez devoir payer
un hébergement sur une instance [Nextcloud](../collaborer/nextcloud.md) mais en plus vous allez devoir
organiser la transition, sauvegarder l'existant, importer les documents
(et les re-trier), apprendre à vos collaborateur⋅rice⋅s comment se
servir de la nouvelle interface\... Bref, pensez que tout cela prend
aussi du temps.
{: .encart }

Dans tous les cas, l'indépendance que vous offrent les logiciels
libres implique des efforts que votre organisation doit anticiper. Mais
attention : la taille de votre organisation ne
présume en rien de ses capacités à effectuer
une bonne transition vers le Libre.

[![formats-ouverts](../img/formats_ouverts.jpg){: .illustration }](https://www.april.org/formats-ouverts-pour-quoi-faire){target=_blank}
*L'échange de fichiers est problématique si  
on ne prend pas en compte la question de  
l'interopérabilité des formats. Tout le  
monde n'utilise pas les mêmes logiciels :  
c'est une liberté, pas une contrainte.  
Extrait de l'affiche [« Formats ouverts,  
pour quoi faire ? »](https://www.april.org/formats-ouverts-pour-quoi-faire), April, 2012.  
Graphisme : Antoine Bardelli      
Licence [Art Libre](http://artlibre.org/)*
{: .figure }
