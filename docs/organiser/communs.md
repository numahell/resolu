---
title: Des communs numériques
---

Participons ensemble à la création de communs
{: .slogan-subtitle }

Un commun est une ressource **partagée et gérée** par
une communauté selon des règles d'utilisation et de gouvernance définies
collectivement.

La gouvernance des communs se fonde sur le **partage**
et la **libre circulation** de ces ressources, qu'elles
soient matérielles ou immatérielles.

Participer à un commun, c'est contribuer à la production ou l'entretien
d'une ressource. Elle est rendue accessible à tou⋅te⋅s.

En tant que membre de la communauté, chacun⋅e a son mot à dire dans les
décisions qui concernent les conditions d'usage, l'organisation et la
gouvernance de la ressource.

Comme le pouvoir est distribué entre toutes les personnes usagères,
l'utilisation de la ressource ne peut pas être ré-appropriée par un⋅e
membre de manière exclusive : pas de décision arbitraire, ni d'unique
responsable.
{: .encart }

Ce modèle social rend possible la création collective de valeur, dont
les conséquences positives s'inscrivent directement dans l'intérêt de la
communauté et de chacun⋅e de ses membres. C'est un modèle fondé sur les
valeurs de contribution, de mutualisation des connaissances, et de
coopération.
{: .slogan-contenu }

## Quelques exemples... de communs numériques

**Wikipédia** : cette encyclopédie collaborative défend
un accès universel au savoir. Par son fonctionnement, les personnes
utilisatrices peuvent apporter leurs connaissances afin de construire
l'une des encyclopédies les plus complètes au monde et traduire les
articles dans plus de 300 langues.

**OpenStreetMap** est une base de données de
cartographie, à laquelle chacun⋅e peut contribuer. Cette coopération
permet d'élaborer une carte du monde précise qui répond aux besoins de
chacun⋅e : chacun⋅e y ajoute son expérience du territoire et participe
ainsi à l'enrichissement des multiples cartes mises à disposition de
tou⋅te⋅s.

Ces outils utilisent tous les deux des logiciels libres, qui sont
eux-mêmes des communs : tout le monde peut les utiliser et participer à
leur amélioration.

## Ce qu'on y gagne ?

**L'accès universel** -- L'accès à un commun numérique
vous est ouvert, le choix de l'utiliser ou non vous appartient. Donner
accès au plus grand nombre, c'est recevoir un plus grand nombre d'idées.

**La distribution du pouvoir** -- En tant qu'usager·ère,
vous pouvez participer aux décisions relatives à l'usage de la ressource
et à l'organisation de la communauté. Les règles sont transparentes et
évoluent par des décisions collectives.

**La robustesse et la pérennité** -- Le modèle des
communs produit des ressources complexes et sophistiquées. Surtout, leur
ouverture permet à tout le monde de les entretenir et de les améliorer ;
au contraire d'une ressource gérée par un seul acteur, la pérennité d'un
commun est assurée par l'évolution de sa communauté.

**La capacitation** -- L'utilisation d'un commun répond
dans un premier temps à un besoin mais la possibilité d'y contribuer
apporte, en plus, une capacitation : on apprend des savoir-faire, on
découvre de nouveaux usages. Autrement dit, on acquiert des capacités
qui augmentent notre pouvoir d'action.

**Le lien social** -- Un commun n'est rien sans une
communauté vivante autour : les rencontres et regroupements qui
permettent de décider collectivement tissent des relations solides entre
usager⋅ère⋅s. S'investir dans une communauté vous procure de belles
surprises ; et si cela ne vous plaît plus, vous êtes libre !

## Solidarité et Collaboration

Les outils numériques propriétaires maintiennent les personnes
utilisatrices dans l'ignorance de leur fonctionnement. À l'inverse,
toute la connaissance accumulée dans un logiciel libre est mise en
commun. Ainsi, personne ne doit repartir de zéro si la solution est déjà
accessible quelque part, et les outils peuvent évoluer pour répondre au
mieux aux besoins !

## Émancipation

Participer aux communs, c'est se donner la possibilité de développer ses
capacités pour construire son autonomie, améliorer son environnement et
ses conditions de vie. L'accès aux données et l'apprentissage de
savoir-faire offrent des opportunités d'action. Ce **libre accès** doit donc être assuré pour garantir que chacun⋅e soit
en mesure de décider et d'agir indépendamment de tout contrôle
arbitraire.

## Le partage et l'inclusion sociale

Qu'il s'agisse d'informations, d'œuvres, de connaissances, de
ressources... les communs sont des vecteurs d'inclusion sociale car ils
permettent à tout le monde d'y accéder librement. La créativité et les
initiatives individuelles au service de l'intérêt collectif émergent
ainsi plus facilement. Cela ouvre une place plus grande à la
**mobilisation collective**, qui est portée par une
**intelligente solidarité**.

## Économie Sociale et Solidaire : œuvrons ensemble !

Les circuits courts, le développement durable, les systèmes d'échange
alternatifs sont des exemples parmi les nombreuses activités
associatives œuvrant pour la prospérité des communs. De la même manière,
les communs numériques ne sont pas que des lignes de codes accessibles
et des règles de modifications transparentes : ils existent grâce à
l'action d'une communauté. C'est ici que l'engagement des associations
est crucial : pour préserver les ressources numériques d'une
réappropriation exclusive et d'un enfermement des usages, **chacun⋅e a un
rôle à jouer** pour soutenir le modèle de gouvernance
qu'il⋅elle souhaite pour ses outils numériques.

## L'engagement

Les outils numériques sont utiles aux mobilisations et facilitent le
« faire ensemble » : valoriser et diffuser ses actions. Le numérique
rend ces actions collectives encore plus efficaces et ouvre de nouveaux
espaces de visibilité. L'engagement associatif est ainsi renforcé.

Agir efficacement pour son association en utilisant des outils
numériques libres, c'est déployer son engagement tout en participant à
la **défense des communs numériques et donc des
libertés** (en particulier la liberté d'expression).
{: .slogan-contenu }

## À vous de jouer !

Utiliser des logiciels libres pour son action associative, c'est
défendre les valeurs associées aux communs et soutenir un modèle de
société où le numérique est au service des citoyens et de l'intérêt
général.

Renforcez votre action en étant partie prenante de la transition vers
les outils numériques libres !
{: .slogan-contenu }

Vous aussi, participez à l'élaboration de communs :

-   utilisez des logiciels libres, donnez votre avis à leurs
    contributeur·rice·s,

-   apportez vos connaissances et votre expertise à des outils déjà
    existants,

-   diffusez vos contenus sous une licence libre, telle que Creative
    Commons ou Art Libre, afin de favoriser la circulation de vos idées
    tout en protégeant votre droit d'auteur,

-   informez également vos membres sur les moyens mis à leur disposition
    pour participer à l'organisation et l'évolution de votre activité
    associative,

-   participez à des traductions de contenu qui vous intéressent,

-   réalisez des tutoriels pour partager vos savoir-faire.

## La lutte contre les inégalités

L'accaparement des outils numériques par quelques-un⋅e⋅s empêche un
libre accès aux ressources numériques. Le modèle propriétaire nourrit
cette dynamique de privatisation des ressources, qui accroît les
inégalités de puissance et les écarts économiques entre acteurs de la
société. Au contraire, les valeurs du Libre s'alignent avec celles de
l'**Économie Sociale et Solidaire**.

## Ne pas oublier

Le droit d'auteur s'applique toujours, quelle que soit l'œuvre. On
pensera bien à ne pas utiliser de contenu dont on ne connaît pas la
licence et à toujours attribuer la paternité des contenus qu'on utilise.

Lorsqu'on change des habitudes, il est nécessaire de mettre en place un
accompagnement dans l'abandon de certains usages et l'adoption de
nouveaux. Face à un nouveau mode de consommation et de participation, il
n'est pas toujours simple de savoir par où commencer ! N'oubliez donc
pas de former les personnes utilisatrices aux nouveaux outils. Il peut
être utile de désigner comme personne référente quelqu'un·e qui a eu le
temps de comprendre l'outil et qui saura aider les autres en cas de
besoin.

## Quelques liens

[wikipedia.org](http://wikipedia.org/)

[openstreetmap.org](http://openstreetmap.org/)

[creativecommons.org](http://creativecommons.org/)

[artlibre.org](http://artlibre.org/)

[framabook.org](http://framabook.org/)
