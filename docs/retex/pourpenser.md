---
title: Dégooglisation : Les éditions Pourpenser
---

Libérer les pratiques, libérer les livres
{: .slogan-subtitle }

*La maison d'édition Pourpenser a attiré notre attention sur Mastodon avec ses prises de position libristes. En discutant un peu nous avons compris qu'elle a joint le geste à la parole en faisant évoluer ses outils informatiques. Ça n'est pas si fréquent, une entreprise qui se dégooglise. Nous lui avons demandé un retour d'expérience.*

(Interview paru sur le Framablog le 19/11/2020)


**Bonjour, peux-tu te présenter brièvement ? Qui es-tu ? Quel est ton parcours ?**

Albert, co-fondateur des éditions Pourpenser avec ma sœur
Aline en 2002. Petit je voulais être garde forestier ou cuisinier...
autant dire que j'ai raté ma vocation :-) (même si j'adore toujours
cuisiner). En 1987 j'avais un voisin de palier qui travaillait chez
Oracle. Après les cours je passais du temps sur un ordinateur qu'il me
mettait à disposition : j'ai donc commencé avec un ordi sur MS-DOS et
des tables SQL. 1987, c'était aussi le tout début de la PAO. Il y avait
un logiciel dont j'ai perdu le nom dans lequel je mettais le texte en
forme avec des balises du genre `<A>ça fait du gras</A>`, je trouvais
ça beaucoup plus intéressant que PageMaker et lorsque j'ai découvert
Ventura Publisher qui mariait les deux mondes, j'ai été conquis.

Par la suite j'ai
travaillé une dizaine d'années dans la localisation de jeux vidéo et de
CD-ROM : nous traduisions le contenu et le ré-intégrions dans le code.
Ma première connexion à internet remonte à 1994 avec FranceNet, j'avais
25 ans. Je découvrais ce monde avec de grands yeux en m'intéressant au
logiciel libre, à la gouvernance d'internet (je me rappelle notamment
de l'ISOC et des rencontres d'Autrans) et ça bousculait pas mal de
schémas que je pouvais avoir. 2000 : naissance de ma fille aînée, je
quitte Paris, je prends un grand break : envie de donner plus de sens à
ma vie. 2002 : naissance de mon fils et création de la maison d'édition
avec ma sœur.

**Tu nous parles de ton entreprise ?**

Dans sa forme, Pourpenser est une SARL classique. Régulièrement, nous nous posons la
question de revoir les statuts mais ça demande du temps et de l'argent
que nous préférons mettre ailleurs. Finalement, le mode SARL est plutôt
souple et dans les faits, nous avons une organisation très...
anarchique. Même si avec Aline nous sentons bien qu'en tant que
fondateurs notre voix compte un peu plus, l'organisation est très
horizontale et les projets partent souvent dans tous les sens.



**Que fait-elle ?**

Dès le départ, nous avons eu à cœur de proposer des livres « avec du
sens ». Aborder des questions existentielles, des questions de sociétés
ou autour de notre relation au vivant. La notion d'empreinte nous
interpelle régulièrement. Ne pas laisser d'empreinte est compliqué.
Mais peut-être pouvons-nous choisir de laisser une empreinte aussi
légère qu'utile ? La cohérence entre le contenu des livres que nous
éditons et la manière dont nous produisons et amenons ces livres aux
lecteurs et lectrices a toujours été centrale... même si rester cohérent
est loin d'être toujours simple. Nous aimons dire que notre métier
n'est pas de faire des livres mais de transmettre du questionnement.
Ceci dit, depuis 2002, nous avons édité environ 120 titres et une
soixantaines d'auteur·e·s. Nous aimons éditer des contes, des romans,
des BD, des jeux, des contes musicaux qui vont amener à une discussion,
à une réflexion. Mais qui sait si un jour nous n'irons pas vers du
spectacle vivant, de la chanson...


**Combien êtes-vous ?**

Normalement, nous sommes 8 personnes à travailler
quasi-quotidiennement sur le catalogue de la maison et cela fait
l'équivalent d'environ 5 temps plein, mais avec la crise actuelle nous
avons nettement plus de temps libre... À côté de ça, nous accompagnons
une soixantaine d'auteur·e·s, travaillons avec une centaine de points
de vente en direct et avons quelques dizaines de milliers de contacts
lecteurs.

**Est-ce que tout le monde travaille au même endroit ?**

L'équipe de huit est principalement située dans l'ouest, et l'une de
nous est du côté de Troyes. Nous nous réunissons environ deux fois par
an et utilisons donc beaucoup le réseau pour échanger. Le confinement de
mars n'a fondamentalement rien changé à notre façon de travailler en
interne. Par contre, les salons et les festivals ou nous aimons
présenter les livres de la maison nous manquent et le fait que les
librairies fonctionnent au ralenti ne nous aide pas.


**Tu dirais que les membres de l'organisation sont plutôt à l'aise avec le numérique ? Pas du tout ? Ça dépend ? Kamoulox ?**

Globalement, la culture « utilisateur du numérique » est bien présente dans toute l'équipe. Mais je dirais
que nous sommes surtout deux : Dominique et moi, que la question de
« jouer avec » amuse. Pour le reste de l'équipe, il faut que ça
fonctionne et soit efficace sans prise de tête.

**Avant de lancer cette démarche, vous utilisiez quels outils / services numériques ?**

Lors de la création en 2002, j'ai mis en place un site que j'avais
développé depuis un ensemble de scripts PHP liés à une base MySQL. Pour
la gestion interne et la facturation, j'utilisais Filemaker (lorsque je
ne suis pas sur Linux, je suis sur MacOS), et au fur et à mesure de
l'arrivée des outils de Google (gmail, partage de documents...) nous
les avons adoptés : c'était tellement puissant et pratique pour une
mini structure éclatée comme la nôtre. Par la suite, nous avons remplacé
Filemaker par une solution ERP-CRM qui était proposée en version
communautaire et que j'hébergeais chez OVH (LundiMatin - LMB) et le
site internet a été séparé en 2 : un site B2C avec Emajine une solution
locale mais sous licence propriétaire (l'éditeur Medialibs est basé à
Nantes) et un site B2B sous Prestashop.

Pour les réseaux
sociaux : Facebook, Instagram Twitter, Youtube. En interne, tout ce qui
est documents de travail léger passaient par Google Drive, Hubic
(solution cloud de chez OVH) et les documents plus lourds
(illustrations, livres mis en page) par du transfert de fichiers (FTP ou
Wetransfer).

**Qu'est-ce qui posait problème ?**

La version
communautaire de LMB n'a jamais vraiment décollé et au bout de 3 ans
nous avons été contraints de migrer vers la solution SaS, et là, nous
avons vraiment eu l'impression de nous retrouver enfermés. Impossible
d'avoir accès à nos tables SQL, impossible de modifier l'interface. À
côté de ça une difficulté grandissante à utiliser les outils de Google
pour des raisons éthiques (alors que je les portais aux nues au début
des années 2000...)

**Vous avez entamé une démarche en interne pour migrer vers des outils numériques plus éthiques. Qu'est-ce qui est à l'origine de cette démarche ?**

La démarche est en cours et bien
avancée. J'ai croisé l'existence de Framasoft au début des années 2000
et lorsque l'association a proposé des outils comme les Framapad,
framacalc et toutes les framachoses ; j'en ai profité pour diffuser ces
outils plutôt que ceux de Google auprès des associations avec lesquelles
j'étais en contact. Mes activités associatives m'ont ainsi permis de
tester petit à petit les outils avant de les utiliser au niveau de
l'entreprise.

Des outils (LMB, Médialibs) propriétaires avec de grandes difficultés et/ou coûts pour
disposer de fonctionnalités propre à notre métier d'éditeur. Des
facturations pour utilisation des systèmes existants plutôt que pour du
développement. Un sentiment d'impuissance pour répondre à nos besoins
numériques et d'une non écoute de nos problématiques : c'est à nous de
nous adapter aux solutions proposées... Aucune liberté.

## « Un besoin de cohérence »


**Quelle était votre motivation ?**

La motivation principale est
vraiment liée à un besoin de cohérence. Nous imprimons localement sur
des papiers labellisés, nous calculons les droits d'auteurs sur les
quantités imprimées, nos envois sont préparés par une entreprise
adaptée, nous avons quitté Amazon dès 2013 (après seulement 1 an
d'essai)... À titre personnel j'ai quitté Gmail en 2014 et j'avais
écrit [un billet sur mon blog](http://www.petigny.com/albert/le-grand-saut/) à ce sujet. Mais
ensuite, passer du perso à l'entreprise, c'était plus compliqué, plus
lent aussi. Par ailleurs nous devions faire évoluer nos systèmes
d'information et remettre tout à plat.

**...et vos objectifs ?**

Il y a clairement plusieurs objectifs dans cette démarche.

- Une démarche militante : montrer qu'il est possible de faire autrement.
- Le souhait de mieux maîtriser les données de l'entreprise et de nos clients.
- Le besoin d'avoir des outils qui répondent au mieux à nos besoins et que nous pouvons faire évoluer.
- Quitte à développer quelque chose pour nous autant que cela serve à d'autres.
- Les fonds d'aides publiques retournent au public sous forme de licence libre.
- Création d'un réseau d'acteurs et actrices culturelles autour de la question du numérique libre.

**Quel lien avec les valeurs de votre maison d'édition ?**

Les concepts de liberté et de responsabilité sont régulièrement présents
dans les livres que nous éditons. Réussir à gagner petit à petit en
cohérence est un vrai plaisir. Partage et permaculture... Ce que je
fais sert à autre chose que mon besoin propre...

**Qui a travaillé sur cette démarche ?**

Aujourd'hui ce sont surtout Dominique et
moi-même qui travaillons sur les tests et la mise en place des outils.
Des entreprises associées : Symétrie sur Lyon, B2CK en Belgique ,
Dominique Chabort au début sur la question de l'hébergement. Un des
problèmes aujourd'hui est clairement le temps insuffisant que nous
parvenons à y consacrer. Aujourd'hui, la place du SI est pour nous
primordiale pour prendre soin comme nous le souhaitons de nos contacts,
lecteurs, pour diffuser notre catalogue et faire notre métier.

**Vous avez les compétences pour faire ça dans l'entreprise ?**

Il est clair
que nous avons plus que des compétences basiques. Elles sont
essentiellement liées à nos parcours et à notre curiosité : si Dominique
a une expérience de dev et chef de projet que je n'ai pas, depuis 1987
j'ai eu le temps de comprendre les fonctionnements, faire un peu de
code, et d'assemblages de briques ;-)

**Combien de temps ça vous a pris ?**

Je dirais que la démarche est réellement entamée depuis 2 ans
(le jour ou j'ai hébergé sauvagement un serveur NextCloud sur un
hébergement mutualisé chez OVH). Et aujourd'hui il nous faudrait un
équivalent mi-temps pour rester dans les délais que nous souhaitons.

**Ça vous a coûté de l'argent ?**

Aujourd'hui ça nous coûte plus car
les systèmes sont un peu en parallèle et que nous sommes passés de
Google « qui ne coûte rien » à l'hébergement sur un VPS pour 400 €
l'année environ. Mais en fait ce n'est pas un coût, c'est réellement
un investissement. Nous ne pensons pas que nos systèmes nous coûteront
moins chers qu'actuellement. Mais nous estimons que pour la moitié du
budget, chaque année, les coûts seront en réalité des investissements.
Les coûts ne seront plus pour l'utilisation des logiciels, mais pour
les développements. Ainsi nous pensons maîtriser les évolutions, pour
qu'ils aillent dans notre sens, avec une grande pérennité.

**Quelles étapes avez-vous suivi lors de cette démarche de dégooglisation ?**


Ah... la méthodologie :-) Elle est totalement diffuse et varie au fil de
l'eau. Clairement, je n'ai AUCUNE méthode (c'est même très gênant par
moment). Je dirais que je teste, je regarde si ça marche ou pas, et si
ça marche moyen, je teste autre chose. Heureusement que Dominique me
recadre un peu par moment. Beaucoup d'échanges et de controverse.
Surtout que le choix que nous faisons fait reposer la responsabilité sur
nous si nous ni parvenons pas. Nous ne pouvons plus nous reposer sur
« c'est le système qui ne fonctionne pas », « nous sommes bloqué·e·s
par l'entreprise ». C'est ce choix qui est difficile à faire. La
démarche c'est les rencontres, les échanges, les témoignages
d'expériences des uns et des autres... Et puis surtout : qu'avons nous
envie de faire, réellement... Dans un premier cas, est-ce que cela me
parle, me met en joie d'avoir un jolie SI tout neuf ? Ou cela nous
aiderait au quotidien, mais aucune énergie de plus. Dans l'option que
nous prenons, l'idée de faire pour que cela aide aussi les autres
éditeurs, que ce que nous créons participe à une construction globale
est très réjouissant... La stratégie est là : joie et partage.



**Au début ?**

Un peu perdu, peur de la complexité, comment trouver les partenaires qui
ont la même philosophie que nous... Mais finalement le monde libre
n'est pas si grand, et les contacts se font bien.

**Ensuite ?**

Trouver les financements, et se lancer.

**Et à ce jour, où en êtes-vous ?**

À ce jour nous avons totalement remplacé les GoogleDrive,
Hubic et Wetransfer par Nextcloud ; remplacé également GoogleHangout par
Talk sur Nextcloud. Facebook, Instagram et Twitter sont toujours là...
Mais nous avons [un compte sur Mastodon](https://mamot.fr/@pourpenser) !
Youtube est toujours là... Mais le serveur Peertube est en cours de
création et Funkwhale pour l'audio également. Concernant
l'administration de ces outils, je suis devenu un grand fan de Yunohost
: une solution qui permet l'auto-hébergement de façon assez simple et
avec communauté très dynamique. Notre plus gros projet est dans le
co-développement d'un ERP open source :
[Oplibris](https://oplibris.org) Ce projet est né en 2018 après une
étude du Coll.LIBRIS (l'association des éditeurs en Pays de la Loire)
auprès d'une centaine d'éditeurs de livres. Nous avons constaté qu'il
n'existait à ce jour aucune solution plébiscité par les éditeurs
indépendants qui ont entre 10 et 1000 titres au catalogue. Nous avons
rencontré un autre éditeur (Symétrie, sur Lyon) qui avait de son côté
fait le même constat et commencé à utiliser Tryton. (je profite de
l'occasion pour lancer un petit appel : si des dev flask et des
designers ont envie de travailler ensemble sur ce projet, nous sommes
preneurs !) Migrer LMB, notre ERP actuel, vers Oplibris est vraiment
notre plus gros chantier. À partir de là, nous pourrons revoir nos sites
internet qui viendront se nourrir dans ses bases et le nourrir en
retour.

**Combien de temps entre la décision et le début des actions ?**

Entre la décision et le début des actions : environ 15 secondes. Par
contre, entre le début des actions et les premières mise en place
utilisateur environ 6 mois. Ceci dit, de nombreuses graines plantées
depuis des années ne demandaient qu'à germer.

## « Nous mettons de grosses contraintes éthiques »


**Avant de migrer, avez-vous testé plusieurs outils au préalable ?**

J'ai l'impression d'être toujours en test. Par exemple,
Talk/Discussion sur Nextcloud ne répond qu'imparfaitement à notre
besoin. Je préférerais Mattermost, mais le fait que Talk/Discussion soit
inclus dans Nextcloud est un point important côté utilisateurs. Nous
mettons de grosses contraintes éthiques, de ce fait les choix se
réduisent d'eux-mêmes, il ne reste plus beaucoup de solutions.
Lorsqu'on en trouve une qui nous correspond c'est déjà énorme !

**Avez-vous organisé un accompagnement des utilisateur⋅ices ?**

L'équipe est assez réduite et plutôt que de prévoir de la documentation
avec des captures écran ou de la vidéo, je préfère prendre du temps au
téléphone ou en visio.

**Prévoyez-vous des actions plus élaborées ?**

Nous n'en sommes pas à ce stade, probablement que si le projet se
développe et est apprécié par d'autres, des formations entre nous
seront nécessaires.

**Quels ont été les retours ?**

Il y a
régulièrement des remarques du genre : « Ah mais pourquoi je ne peux
plus faire ça » et il faut expliquer qu'il faut faire différemment.
Compliqué le changement des habitudes, ceci dit l'équipe est bien
consciente de l'intérêt de la démarche.

**Comment est constituée « l'équipe projet » ?**

Dominique, B2CK, Symétrie.

**Quelles difficultés avez-vous rencontrées ?**

La difficulté majeure est de
trouver le bon équilibre entre la cohérence des outils et l'efficacité
nécessaire dans le cadre d'une entreprise. Le frein majeur côté
utilisateurs est de faire migrer les personnes qui utilisent encore
Gmail pour le traitement de leur courriel. L'interface est si pratique
et la recherche tellement puissante et rapide qu'il est compliqué de le
quitter. Une autre difficulté est d'ordre comptable et financier :
comment contribuer financièrement à ce monde du logiciel libre ? Comment
donner ? A quelles structures ? (aujourd'hui nos financements vont
principalement au développement de Tryton).

**Et l'avenir ? Envisagez-vous de continuer cette démarche pour l'appliquer à d'autres aspects de votre organisation ?**

Côté création, j'aimerais beaucoup
que nous puissions utiliser des outils libres tels que Scribus,
Inkscape, Krita ou GIMP plutôt que la suite Adobe. Mais aujourd'hui ces
outils ne sont pas adoptés par l'équipe de création car trop compliqués
d'utilisation et pas nativement adaptés à l'impression en CMJN. Une
alternative serait d'utiliser la suite Affinity (mais qui n'est pas
open source...)



**Quels conseils donneriez-vous à d'autres organisations qui se lanceraient dans la même démarche ?**

Y prendre du plaisir ! Mine de rien, la démarche demande du
temps et de l'attention. Il faut confier ça à des personnes qui
prennent ça comme un jeu. Oubliez la notion de temps et de délais, optez
pour les valeurs qui soient plus la finalité et le plaisir. Au pied de
la montagne entre prendre le téléphérique ou le chemin à pied ce n'est
pas le même projet, vous n'avez pas besoin des même moyens.

**Le mot de la fin, pour donner envie de migrer vers les outils libres ?**

Sommes-nous les outils que nous utilisons ? Libres ? Quitter les réseaux
sociaux centralisés est extrêmement complexe. Je manque encore de
visibilité à ce sujet et ça risque d'être encore très long. J'ai
proposé à l'équipe une migration totale sans clore les comptes mais
avec un mot régulièrement posté pour dire « rejoignez-nous ici plutôt
que là ». Mon rêve serait d'embarquer au moins une centaine
d'entreprises dans une telle démarche pour tous faire sécession le même
jour. Des volontaires ? :-)

## Aller plus loin


-   Fair-play, Albert ne nous l'a jamais demandé, il sait qu'on est
    (~~un peu~~ allergique à la pub, mais on vous donne quand même le
    lien vers [le site des Éditions Pourpenser](https://www.pourpenser.fr/)
