---
title: Dégooglisation : le grap, une coopérative libérée
---

La stratégie de fait pas tout. Il faut du temps humain et des choix collectifs.
{: .slogan-subtitle }


*Le [GRAP](https://www.grap.coop/) (Groupement Régional Alimentaire de Proximité), une coopérative réunissant des activités de transformation et de distribution dans l'alimentation bio-locale, a publié le [récit de sa dégooglisation](https://www.grap.coop/degooglisation/) sur son site web. Avec leur autorisation, nous reproduisons ici ce long texte en trois parties pour vous partager leur expérience.*

(Textes parus sur le Framablog en février 2023.)

# La dégooglisation du GRAP, partie 1

De 2018 à cette fin 2022, nous avons travaillé à Grap à notre dégooglisation. Nous vous proposons *ce long texte en trois parties* pour vous partager notre expérience. Son premier intérêt est de laisser une trace du travail fourni et d'en faire le bilan. Le deuxième intérêt est de partager cette expérience à d'autres structures qui souhaiteraient se lancer dans l'aventure. Nous partageons dans ce texte les processus mis en place, les différentes étapes de cette dégooglisation, les difficultés rencontrées et quelques conseils. Pour toute question ou retour, vous pouvez contacter le pôle informatique de Grap : *pole-informatique \<arobase\> grap.coop*

**Bonne lecture et longue vie aux outils numériques émancipateurs et Libres !**

## Au début de Grap en 2012...


Il y a 10 ans, Grap naissait en tant que [SCIC](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_coop%C3%A9rative_d%27int%C3%A9r%C3%AAt_collectif) (Société Coopérative d'Intérêt Collectif). En 2012 est écrite une 1ère version du préambule des statuts qui décrit l'intérêt collectif qui réunit les associé·e·s de la SCIC. Ce préambule présentait alors que Grap aller « Contribuer au développement d'activités économiques
citoyennes et démocratiques, c'est-à-dire (...) travaillant dans une logique de partage des savoirs, en phase avec la philosophie Creative Commons ». Cette 1ère référence au monde du Libre est **complétée et enrichie 5 ans plus tard** à l'occasion d'une révision du préambule des statuts, en 2017. Désormais le préambule des statuts indique que
Grap entend :

> *Contribuer au développement d'activités économiques citoyennes et
> démocratiques* (...) promouvant l'économie des biens communs,
> c'est à dire :
>
> - Travailler dans une logique de partage des savoirs, en phase avec la philosophie Creative Commons
> - Promouvoir, contribuer et utiliser des logiciels libres au sens de la Free Software Foundation ; minimiser l'utilisation de logiciels sous licences privatives
> - Promouvoir, contribuer et utiliser des solutions informatiques qui n'exploitent pas de façons commerciales les données des utilisateurs et qui respectent leurs vies privées

Notre démarche de dégooglisation s'inscrit donc dans la continuité des choix politiques portés par les associé·e·s de la coopérative depuis sa création. Par dégooglisation, nous entendons ici le remplacement des logiciels propriétaires — qu'ils soient détenus par les GAFAM ou non — par des [logiciels Libres](https://fr.wikipedia.org/wiki/Logiciel_libre).

Dès le début, il est décidé d'internaliser une partie de l'informatique au sein de l'équipe qui rend les services aux activités de la coopérative. (À Grap, nous utilisons le terme d'*activité* pour désigner les entreprises associées à Grap et les activités économiques de la Coopérative d'Activités et d'Emploi). La majorité du temps
informatique sera dédié au développement du [progiciel](https://fr.wikipedia.org/wiki/Progiciel) libre OpenERP (nommé désormais Odoo) pour gérer la première activité d'épicerie ([3 P'tits Pois](https://www.3ptitspois.fr/) à Lyon) de la coopérative. Par pragmatisme économique et choix stratégique, les autres outils de la coopérative ne sont pas choisis par le critère de logiciel Libre ou non. Ainsi, la coopérative va utiliser Google Drive, Google Mail, Google Agenda, et aussi d'autres logiciels spécifiques comme EBP pour la compta ou Cegid Quadra pour la paie.

## 2018-2020/ Sortir de Google Drive pour Nextcloud


Après le départ d'un des cofondateurs et d'un informaticien en 2014, le service informatique va fonctionner avec 1 seule personne jusqu'à fin 2017. Sylvain Le Gal va alors consolider le périmètre existant (gestion d'une eBoutique, développements spécifiques à l'alimentaire dans OpenERP, connexion avec des balances client·es et migration de OpenERP 7.0 à Odoo 8.0). Fin 2017, l'embauche de Quentin Dupont permet de gagner en temps de travail disponible et d'agrandir le périmètre des services du pôle informatique.

## L'été 2018 pour valider l'alternative à Google Drive

Le choix du logiciel remplaçant se fait très facilement : Nextcloud est *LA* solution Libre qui s'impose autant par sa prise en main relativement simple pour des utilisateur·ices de tout niveau, que par l'engouement de sa communauté et son administration alors maîtrisée par le pôle informatique. Il faut quand même s'assurer que toutes les fonctionnalités utilisées actuellement trouvent leur équivalent. Grâce aux différentes applications existantes sur Nextcloud, les différents besoins se retrouvent bien couverts.

![](../img/Tableau-spec-Nextcloud.png)

## À l'automne 2018, on prend la décision de sortir de Google Drive

Un changement de logiciel peut être l'occasion de revoir ses pratiques. Nous en profitons pour revoir *notre arborescence de fichiers et de dossiers*. Nous créons alors :

- Un compte Nextcloud par :
    - personne physique de l'équipe interne
    - personne physique des activités qui ont des mandats particuliers (administrateur·ice au CA par exemple)
    - activité de la coopérative (donc par « personne morale ») et non pas par personne physique de la coopérative pour différentes raisons :
        - de nombreuses activités partagent réellement leurs ordinateurs tout au long de la journée
        - aucun intérêt à ce que chaque personne ait son compte,
        - cela rajouterait une dose énorme de suivi de création de compte, de support, etc.
        - ce choix vient avec une limite : l'accès aux documents personnels avec le pôle social n'est pas possible
- Un « groupe » Nextcloud pour chaque groupe autonome
    - un groupe par pôle de l'équipe interne
    - un groupe par mandat : DG, CA
    - un groupe par activité de la coopérative - regroupant le compte de la personne morale + les comptes des personnes physiques de cette activité qui ont des mandats particuliers.
- Des dossiers communs pour travailler collaborativement
    - entre pôles de l'équipe
    - entre membres de la coopérative
    - entre mandataires (DG ou CA)



![La structure de dossiers présentée en nov. 2018 et qui est en place depuis](../img/representation-dossiers-NC.png)


Avec Nextcloud, nous avons donc pu créer une architecture plutôt *simple pour les utilisateur·ices* mais permettant de *répondre aux complexités du travail collaboratif* entre des profils bien différents. Grâce aux droits d'accès paramétrables finement, le Nextcloud permet ainsi d'offrir *plus de transparence et de collaboration* dans la coopérative, que ce soit par les dossiers partagés totalement ou, à l'inverse, les dossiers dont l'accès n'est possible qu'en lecture sans possibilité de modifier.

## 2019-2020 : la dégooglisation de 150 personnes dans 50 activités

Google Drive n'est pas seulement utilisé par l'équipe interne. L'outil est partagé à l'ensemble de la coopérative. C'est à dire à une cinquantaine — à l'époque - d'activités indépendantes, allant de l'entrepreneuse seule à la petite équipe de 10 personnes. Il faut donc embarquer tout le monde dans ce changement.

- *Politiquement/théoriquement* pas de soucis. Les méfaits de Google sont connus de la majorité des gens et théoriquement, nous n'avons jamais eu de désaccords sur l'idée de sortir de Google Drive.
- *En pratique*, Google Drive s'avère être plutôt lourd à l'utilisation, pas bien maîtrisé ni maîtrisable, surtout concernant la gestion des partages qui est un véritable enfer (« Qui est le fichu propriétaire de ce fichier dont le propriétaire originel est parti de la structure / n'a plus de compte Google ? »).

En allant sur Nextcloud, nous allions maîtriser - et donc être responsables - des données de la coopérative, *nous allions retrouver de la souveraineté* et de la compétence sur le sujet. Au printemps 2019, nous changeons aussi d'outil de documentation. Pour sa simplicité d'utilisation et son ergonomie générale, nous choisissons le logiciel Libre [BookstackApp](https://www.bookstackapp.com/). Depuis, notre *librairie* tourne toujours aussi bien et héberge notre documentation informatique mais aussi toute la documentation stable de la coopérative.
Depuis 2020, la documentation informatique est librement consultable ici : [librairie.grap.coop/shelves/informatique](https://librairie.grap.coop/).

## Une première difficulté : l'export des données de Google

L'export fut en effet très compliqué, trop compliqué pour un logiciel conçu par l'une des entreprises les plus puissantes au monde. L'export des données d'un Google Drive (à l'époque en tout cas) *est extrêmement long et très peu sécurisant* : Google fournit l'export en archives coupées en plusieurs parties (du style `ARCHIVE-PART01` `ARCHIVE-PART02`), *archives dont une partie... pouvait être manquante* (ex : on a la partie 01, 02, 04, 05 mais pas la partie 03), nécessitant de refaire un export entier. Nous avons donc passé de nombreuses heures à exporter les données, puis nous les avons sécurisées sur un disque dur externe, avant de les envoyer sur notre Nextcloud.

## Et tu formes formes formes, c'est ta façon d'aimer

Pour réussir à dégoogliser la coopérative, pas de miracle, on a enchaîné la formation des activités une à une, en mutualisant des formations par territoire géographique. Chaque formation durait environ 1h30. En 2019, nous avons passé environ *150 heures de travail* à la formation, l'accompagnement et la documentation de cette étape de dégooglisation (+ les heures techniques, voir bilan financier à la fin de ce récit). L'ensemble de la documentation - qui est un travail continu - est consultable ici : [librairie.grap.coop/books/nextcloud](https://librairie.grap.coop/books/nextcloud). En janvier 2020, soit plus d'un an après la décision de passer sur Nextcloud, la migration était officiellement finie !

## Une difficulté pas anticipée : les limitations d'Onlyoffice pour les commandes groupées

Tout allait bien dans la dégooglisation progressive de la coopérative. Au cours de l'année 2019, la moitié de la coopérative utilise désormais Nextcloud au lieu de Google Drive ! Un des avantages de la coopérative pour les activités est de pouvoir mutualiser de nombreux sujets. Un de ces sujets est l'approvisionnement en produits artisanaux en circuits courts grâce à une logistique interne - Coolivri. Cette logistique s'appuyait à l'époque sur un GROS fichier tableur en ligne sur Google Drive. Le 2 août 2019, une première commande groupée d'oranges et d'agrumes est lancée sur le Nextcloud et toutes les prochaines commandes groupées vont débarquer sur le Nextcloud, géré par l'application Onlyoffice. Et c'est vers cette période que l'on se rend compte que *l'application disponible d'Onlyoffice a une limitation : pas plus de 20 personnes connectées simultanément sur l'ensemble des fichiers collaboratifs du nuage !* À l'époque nous devions avoir une soixantaine d'utilisateur·ices et une équipe interne qui l'utilise toute la journée : ce n'était pas tenable.

Cette limitation n'est pas technique, mais bien un choix délibéré de l'entreprise développant le logiciel pour amener à payer une licence permettant d'accéder au logiciel sans limitation. Un modèle [freemium](https://fr.wikipedia.org/wiki/Freemium) en soi. Cette question du modèle économique et de ce qu'est un « vrai » logiciel libre est bien sûr compliqué, et amènera de nombreux débats dans les forums de discussion de Nextcloud.

Fin 2019, nous nous questionnons réellement sur le fait de payer cette licence (coût à l'époque : environ 1500€ en une fois pour 100 utilisateur·ices simultanées).

Après avoir écumé les Internets, contacté toutes les structures amies qui auraient la même problématique, la solution vient finalement [de la communauté](https://help.nextcloud.com/t/onlyoffice-compiled-with-mobile-edit-back/79282) elle-même qui est partagée sur le fait de contourner cette limitation qui constitue le modèle économique de l'entreprise développant Onlyoffice. Un développeur bénévole a réussi à reproduire le logiciel
(légalement car le logiciel est Libre) en enlevant cette limitation !

Depuis, nos commandes groupées ont été rapatriées sur Odoo grâce à un gros développement interne, en faisant un outil beaucoup plus résilient et solide. Et nous continuons d'utiliser Onlyoffice dans des versions communautaires trouvées par ci par là.


## To be continued

Dans la seconde partie, nous continuerons notre récit de dégooglisation, nous permettant de nous débarrasser de Google Agenda puis du mastodonte : Gmail !

# La dégooglisation du GRAP, partie 2

En janvier 2020, après plus d'un an à avoir pris la décision de passer sur Nextcloud en remplacement de Google Drive, la migration était officiellement finie ! Mais voilà, nous passions encore pas mal de temps à ouvrir un onglet Google pour consulter nos agendas, ainsi que nos mails pour les personnes utilisant Gmail en ligne.

## 2021 : fini Google Agenda, go Nextcloud Agenda

Fin septembre 2020, nous décidons collectivement de passer sur l'agenda Nextcloud. *Nous nous laissons 3 mois pour commencer l'année 2021 sur le nouvel outil*. Quelques personnes (notamment le pôle informatique) vont alors tester en conditions réelles Nextcloud Agenda. *Le challenge est sympa car nous décidons de faire ça en pleine migration d'Odoo de version 8 à la version 12, qui est le résultat de pas moins de 1000 heures de temps de travail et 294 tests de non régression.* L'export de données de Google Agenda se passe relativement bien, et l'import sur Nextcloud Agenda aussi. Les seuls soucis viennent de soucis d'exportation d'évènements récurrents du côté Google. On demande alors de recréer ces évènements du côté de Nextcloud Agenda. Début 2021, la migration n'est pas possible pour trop de monde dans l'équipe : nous décidons de nous donner du mou et de fixer une date de bascule au 29 mars 2021 après que certains temps collectifs soient passés (l'assemblée générale notamment). *Une procédure est écrite pour que chaque personne s'autonomise dans sa migration, mais la majorité de la migration se fait collectivement à la date choisie du 29 mars :*

- export de l'agenda Google
- import dans l'agenda Nextcloud
- partage de son agenda au reste de l'équipe
- (optionnel) synchronisation de l'agenda avec Thunderbird
- création des agendas partagés pour les salles de réunion

Tout est documenté ici : [librairie.grap.coop/books/nextcloud/page/agenda-nextcloud](https://librairie.grap.coop/books/nextcloud/page/agenda-nextcloud).

Depuis avril 2021, nous sommes donc officiellement toustes sur Nextcloud Agenda. L'application reçoit régulièrement des mises à jour porteuses de fonctionnalités bien chouettes (corbeille, recherche d'évènements, recherche d'un créneau de disponibilité), ou de corrections de bugs.

## 2021-22 : la transformation complète : sortir de Gmail

Nous voilà arrivé·es à la dernière étape qui nous permet de sortir des outils Google pour l'équipage (nouveau nom de l'équipe interne). La plus dure. Même si cette étape ne concerne « que » les membres de l'équipage, *cette transformation fut la plus longue à mener. Pourquoi ? Parce que :*

- le mail est l'outil principal de la majorité des salarié·es de l'équipe qui l'utilisent toute la journée
- Gmail est très performant, notamment dans la recherche de mail
- certain·es personnes ont jusqu'à 10 ans d'habitudes de travail avec Gmail

D'ailleurs, on l'a constaté empiriquement, les personnes les plus anciennes de Grap furent les personnes les plus compliquées à faire transiter. Autant du point de vue technique (transférer 10 ans de mail est forcement plus compliqué que pour une personne arrivée récemment) que des habitudes prises sur le logiciel.

*Conseil num. 1 : plus on s'y prend tôt à se dégoogliser, moins ça sera compliqué dans la conduite du changement de logiciel.*

## Été 2021 : trouver la solution technique remplaçante

### Gandi pour la gestion de l'hébergement de mail

Nous travaillons avec Gandi (gandi.net) pour la majorité des activités de Grap afin de gérer leur nom de domaine et leurs mails. Pourquoi Gandi ?

- Gandi est engagé depuis longtemps dans le respect de la vie privée
- Gandi est une entreprise qui roule à priori bien sur laquelle on peut compter sur la durée
- Gandi a un support de qualité qui répond rapidement à toutes nos demandes (et ce fut bien utile lors des moments de doute technique pour cette dégooglisation)
-   Gandi est une entreprise française qui paye à priori ses impôts en France...

### Thunderbird comme logiciel bureau

Thunderbird va être notre *pierre angulaire* pour cette dé-gmail-isation. Autant pour permettre le transfert des mails de Google à Gandi, que pour travailler ses mails pour la suite. Ce fut une évidence de partir sur Thunderbird au début.

- Ce logiciel libre est complet. Peut-être même trop complet, ce qui rend son ergonomie critiquable.
- Ce logiciel est aussi assez ancien, ce qui lui donne une bonne robustesse. Peut-être trop ancien, ce qui rend son ergonomie critiquable
- Ce logiciel a une communauté importante qui développe de *très nombreux modules complémentaires* qui viennent se greffer à Thunderbird pour apporter une myriade de possibilités.

Quelques mois plus tard, après la prise en main de certain·es utilisateur·ices, et de leur critique légitime, on s'est senti obligé de réaliser un banc d'essai (*benchmark*), qui validera définitivement ce choix.

![Le benchmark pour choisir notre logiciel de bureau pour la gestion des emails](../img/benchmark-client-mail.png)

Les critères suivants ont été retenus :

- logiciel libre
- fonctionne sur Linux Ubuntu et Windows
- communauté vivante et grande
- modèle économique viable
- installation simple
- rempli les fonctionnalités de base demandées par les collègues (voir plus tard dans le texte)


**Mais pourquoi un logiciel de bureau ?** Gandi propose en effet deux logiciels en ligne, deux webmails noméms SOGo et Roundcube. Ces logiciels ont des interfaces très différentes mais font sensiblement la même chose en termes de fonctionnalités. Cependant ces logiciels ne proposent pas toutes les fonctionnalités utilisées dans un contexte professionnel (voir les fonctionnalités demandées. SOGo est lourd à l'utilisation et Roundcube a une ergonomie trop ancienne. La comparaison avec Gmail n'est pas flatteuse et ne permettra pas d'embarquer des fervent·es utilisateur·ices de Gmail. Par ailleurs, il nous fallait une solution pour exporter les mails de Gmail à Gandi, et Thunderbird est le logiciel candidat idéal, permettant d'avoir conjointement deux boîtes mails l'une à côté de l'autre, permettant notamment de transférer facilement ses mails.
{: .encart }


## Automne 2021 : identifier les besoins et fonctionnalités utilisées

Pour être certain de pouvoir sortir de Google, il faut s'assurer que les collègues vont retrouver leurs petits, ou que l'on assume collectivement que l'on perdra des usages / fonctionnalités en passant sur Thunderbird. Pour cela, nous envoyons un sondage ([nuage.grap.coop/apps/forms/qQ9FQowWZfYsjs2n](https://nuage.grap.coop/apps/forms/qQ9FQowWZfYsjs2n)) qui nous permet d'y voir plus clair sur les fonctionnalités utilisées par
l'équipe pour ajuster nos formations, documentations et recherches de
modules complémentaires dans Thunderbird.

![Réponses à la question « Quelles fonctionnalités mail utilises-tu actuellement ? »](../img/fonctionnalites-mails-les-plus-utilisees.png)



![Réponse à la question « Quelles fonctionnalités mail AIMERAIS-tu découvrir ou utiliser ? »](../img/fonctionnalites-mails-a-decouvrir.png)


Sur la question « Sur une échelle de 0 à 6, est-ce que tu souhaites être précurseur·se de ce changement ?
*(0 : non / 6 : trop chaud·e)* », la moyenne et la médiane est à 3,5.

Les gens sont donc... moyennent chaud·es en général !

Voici les points les plus bloquants pour un passage sur Thunderbird selon notre analyse :

- les mails ne sont pas gérés sous la forme de fils de conversation (*NdE : pour l'heure (février 2023) en fait la vue par conversation de Thunderbird n'offre pas de fonctionnalités similaires*)
- la recherche Thunderbird est laborieuse et pas aussi précise et rapide que Gmail
- la peur de perdre des mails anciens
- l'ergonomie de Thunderbird, notamment la différence de fluidité par rapport à une page web comme Gmail

*Pour réussir ce changement de logiciel, il faut que les étapes soient claires et transparentes pour les utilisateur·ices*. Cela leur permet de se projeter : « ok dans 6 mois / 1 an je change d'outil et je sais à
peu près ce qui m'attend ! ». Après ce premier sondage, un calendrier a donc été partagé, indiquant les différentes dates menant à la dégooglisation de tout le monde.

## Automne - Hiver 2021 : formation et documentation Thunderbird

4 personnes sur 20 utilisent déjà Thunderbird. Pour les 16 autres, nous prévoyons d'étaler les formations par petits groupes sur 3 mois : les personnes les plus intéressées commencent dès mi-octobre, et les personnes les plus frileuses seront formées en janvier, ce qui nous laissera le temps d'avoir des retours, d'ajuster la formation et la documentation. La formation suit le programme que vous pouvez retrouver ici : [librairie.grap.coop/books/mail/page/plan-de-presentation-thunderbird](https://librairie.grap.coop/books/mail/page/plan-de-presentation-thunderbird) :

- une aide à l'installation de Thunderbird et du paramétrage du compte Gmail
- une présentation globale de l'outil
- une présentation des fonctionnalités de base
- des conseils globaux d'utilisation et la présentation des meilleurs modules complémentaires.

La documentation va jouer un rôle très important dans la dégooglisation. Et dès septembre, on va mettre le paquet pour tout bien documenter.

- *Dégooglisation - sortir de Gmail :* [librairie.grap.coop/books/mail/chapter/degooglisation-sortir-de-gmail](https://librairie.grap.coop/books/mail/chapter/degooglisation-sortir-de-gmail)
- *Tutos Thunderbird :* [librairie.grap.coop/books/mail/chapter/tutos-thunderbird](https://librairie.grap.coop/books/mail/chapter/tutos-thunderbird)

Ce travail de plusieurs mois va être itératif : chaque formation apporte son lot de questions, ou de bugs, ou de besoins qu'il faut alors ocumenter et faire repartager à tout le monde. De nombreux points mails (ou des messages informels) sont envoyés à l'équipe pour leur faire part des retours, de l'avancée et des nouveaux modules complémentaires ou paramétrages trouvés pour faciliter l'utilisation de Thunderbird.


### Une difficulté anticipée mais relou : le lien Thunderbird - Gmail

Thunderbird a des défauts indéniables. Mais dans cette dégooglisation, on n'est pas aidé par Gmail qui aime bien avoir des comportements... embêtants. Une de ses particularités est le traitement des mails dans un
dossier appelé « Tous les messages ». Pour citer la [doc officielle](https://support.mozilla.org/fr/kb/thunderbird-et-gmail#w_abonnement-aux-dossiers-et-synchronisation) de Thunderbird :

> Tous les messages : contient *une copie* de tous les messages de
> votre compte Gmail, en incluant le dossier « Courrier entrant », le
> dossier « Envoyé » et les messages archivés.

Donc si vous avez 10 000 messages entrants et sortants, Thunderbird va télécharger 20 000 mails. Sachant qu'on retrouve tous ses mails dans *Courrier entrant* et *Envoyés*, ce dossier ne sert donc à rien. Après plusieurs semaines d'utilisation, et certains ralentissements au lancement de Thunderbird, nous avons fini par conseiller aux gens de se *désabonner* de ce dossier. D'autres conseils seront documentés par la
suite ici : [librairie.grap.coop/books/mail/page/thunderbird-et-gmail](https://librairie.grap.coop/books/mail/page/thunderbird-et-gmail)


## Avril 2022 : premier bilan et questionnement technique

Le calendrier des formations a été quasiment tenu. C'est seulement en janvier que certaines formations n'ont pas eu lieu, du fait de difficultés professionnelles rencontrées dans certains pôles de l'équipe. Il ne restait alors que 2 personnes à former. Mais entre temps, Quentin qui est responsable de cette dégooglisation, est parti en congés sans solde en février-mars. La décision avait été prise de ne pas se presser avant son départ et de faire le point en avril, nous y voilà.

- 2 personnes non formées en janvier + 2 arrivées
- Certaines personnes de l'équipe n'ont pas pris le pli et sont revenues un peu / beaucoup sur Gmail
- Un tableau partagé a fait remonter les problèmes soulevés :
    - La plupart peuvent être réglés par contournement ou par une meilleure documentation.
    - La recherche de mails est laborieuse.

Nous décidons de :

- former les gens qui ne l'ont pas été
- continuer à documenter et informer [des meilleurs modules](https://librairie.grap.coop/books/mail/page/le-top-des-modules-complementaires) et [petits paramétrages qui changent la vie](https://librairie.grap.coop/books/mail/page/optimisation-et-petits-parametrages)
- s'interroger sur pourquoi certaines personnes n'ont pas pris le pli
- demander l'avis des membres de l'équipe sur Thunderbird et la dégooglisation en cours
- faire un *benchmark* des solutions (voir si Thunderbird est vraiment le cheval gagnant)
- s'assurer et valider le processus technique de bascule qu'il faudra faire (le voici : [librairie.grap.coop/books/mail/page/passer-de-google-mail-a-gandi-point-de-vue-adminsys](https://librairie.grap.coop/books/mail/page/passer-de-google-mail-a-gandi-point-de-vue-adminsys))
- prendre une décision lors de notre comité de pilotage informatique qui arrive

*Conseil num. 2 : Nous prenons aussi la décision que Quentin ne soit pas le seul à porter ce projet. Il ressent une charge mentale et une certaine pression à gérer les retours des personnes en difficulté. Pour ne pas non plus tomber dans une posture de l'informaticien libriste qui impose le choix, et pour bien affirmer que nous prenons des choix collectivement, nous allons dé-personnifier le projet. Désormais le travail sera soutenu et partagé avec Sandie, et les mails signés par le pôle informatique.**

## Mai 2022 : la recherche boostée à notre rescousse !

Enfin ! Nous avons trouvé un moyen de répondre aux soucis de recherche sur Thunderbird. Avec un habile mélange de *dossier virtuel* et d'un module complémentaire de recherche avancée, nous parvenons à lier rapidité et complexité de recherche ! Nous le documentons dans la partie 4 de ce tuto : [librairie.grap.coop/books/mail/page/recherche-mail-booste](https://librairie.grap.coop/books/mail/page/recherche-mail-booste).


## Juin 2022 : deuxième bilan : on y va, on sort de Google ?

Notre comité de pilotage ne prend pas une décision ferme. On continue juste à valider de travailler sur cette dégooglisation. En dehors de tous les aspects politiques, en sortant de Google, nous allons cesser de payer 2000€/an pour les comptes pros que nous avons, et c'est toujours ça de gagné dans un moment de crise économique ! Deux mois plus tôt, nous avions envoyé [un formulaire](https://nuage.grap.coop/apps/forms/T5kowxs9cPg9ECDr) à l'équipe, commenté par cette phrase qui résume son intention : *« Vive le consentement, à bas la coercition »*, pour prendre la température de l'équipe sur l'utilisation de Thunderbird. Voici notre analyse résumée des résultats :

Les personnes n'ayant pas encore franchi l'étape Thunderbird sont :

-   une grande partie d'un pôle en surcharge
-   les « ancien⋅nes » qui sont là depuis longtemps

Les difficultés principales vis-à-vis de l'outil sont :

- la recherche de mail
- le changement d'usage ergonomique
- des problèmes liés à la connexion avec Google
- des besoins spécifiques non fonctionnels (invitation Outlook)
- des problèmes spécifiques réglés depuis (soucis d'antivirus, paramétrage mail d'absence, etc.)

Cependant :

- L'équipe est chaude pour sortir de Google !
- L'équipe se sent bien accompagnée à ce changement.

Pourtant :

- une minorité de l'équipe (3 à 4 personnes) ne se sent pas sécurisée ou perd quelques minutes par jour à
l'utilisation de Thunderbird. Ces 3 ou 4 personnes se recoupent avec les personnes utilisant Gmail. Nous pensons qu'avec l'usage et les améliorations du logiciel, nous parviendrons à améliorer ça.

Les personnes revenues sur Gmail l'expliquent par :

- « la flemme »
- un mauvais timing ou un mauvais paramétrage au début
- pôle ou personne avec grosse charge de travail

Nous décidons alors :

- de réaliser deux sessions de formation à la recherche boostée([voir librairie.grap.coop/books/mail/page/recherche-mail-booste](https://librairie.grap.coop/books/mail/page/recherche-mail-booste))
- de travailler sur la solution d'application smartphone adéquate pour sortir de l'application Gmail
- de redonner une formation aux 5 personnes qui n'ont pas fait le switch afin qu'elles y arrivent
- de fixer la date de sortie de Google : cela sera la 1ère ou 2ème semaine d'août
- de commencer à créer toutes les boîtes mails et redirections mails nécessaires

*Conseil num. 3 : nous avions 17 boîtes mails à recréer et 80 redirections de mails assez complexes à réaliser. C'est un travail fastidieux qui demande de se concentrer pour ne pas louper un mail dans la redirection mail créée. Car non, il n'existait pas d'export Google des « groupes Google » que nous utilisions. Le conseil est donc le suivant : partagez le travail :) Merci Sandie pour ce gros taf !*

## Juillet 2022 : la bonne nouvelle, Thunderbird s'améliore

Alors que nous venions de fixer le créneau de départ de Google (début août), Thunderbird sort sa dernière version (la 102), le 29 juin. Cette version apporte de très nombreuses améliorations ergonomiques, rendant le logiciel bien plus agréable à utiliser. Et quand on utilise un logiciel toute la journée, ce n'est pas un petit détail que de pouvoir modifier la taille d'affichage, la taille de police, les couleurs des dossiers mails ou encore une gestion des contacts totalement re-désignée. [Leur annonce officielle est publiée sur le blog Thunderbird](https://blog.thunderbird.net/2022/06/thunderbird-102-released-a-serious-upgrade-to-your-communication/).

Et les bonnes nouvelles s'enchaînent :

- [Thunderbird annonce](https://blog.thunderbird.net/2022/06/revealed-thunderbird-on-android-plans-k9/) rejoindre le projet K-9 Mail pour une application libre sur Android qui va donc s'améliorer encore plus vite !
- Et [leur feuille de route de modifications futures](https://developer.thunderbird.net/planning/roadmap) sont très très prometteuses pour répondre aux soucis les plus courants :
    - des fils de conversations natifs !
    - une ergonomie qui s'améliore de jour en jour avec notamment l'affichage des mails sur plusieurs lignes
    - une synchronisation de son compte qui permettrait d'avoir deux Thunderbird sur deux ordis différents

## 9 Août 2022 : le fil rouge sur le bouton rouge

Depuis quelques mois, on discutait avec Gandi pour nous assurer que la procédure était la bonne. Quel plaisir d'avoir des gens qui répondent rapidement à ces demandes. Merci ! Nous étions donc plutôt prêts pour ce switch. Le mardi 9 août à 22h00, alors que les collègues sont pour la plupart en vacances, on change les DNS du domaine `grap.coop` (DNS = règles techniques qui disent ce qui se passe avec `grap.coop`) pour débrancher Google et brancher Gandi. *Le mardi 9 août à 23h50*, après quelques tests d'envoi et de réception de mails, j'annonce officiellement que tout semble fonctionner comme prévu. Les mails de Gandi partent bien. On reçoit bien les mails sur la nouvelle boîte mail. Le monde n'a pas cessé de tourner. Victoire !

### Une difficulté pas anticipée : l'envoi de mail par notre logiciel Odoo

En créant toutes les boîtes mails sur Gandi, nous nous étions rendu compte des cas particuliers (des personnes qui avaient un compte mail mais qui n'étaient pas ou plus dans l'équipe par exemple) mais ce n'est que tardivement qu'on a réalisé que la boîte mail `serveurs -arobase- grap.coop` servait de boîte d'envoi à l'ensemble des mails du logiciel Odoo utilisé par les 65 activités. Comment cela allait se comporter en passant chez Gandi ? Deux soucis sont encore en cours :

#### 1) L'usurpation d'identité

- En fait, chaque activité envoie ses bons de commandes et factures depuis Odoo. Odoo utilise une seule boîte mail `serveurs grap.coop` mais lors de l'envoi, prend l'identité de l'activité qui envoie un mail.
- Cette « usurpation d'identité » était bien acceptée car nous étions chez Google. Mais avec le passage chez Gandi, cette usurpation d'identité n'est plus acceptée par les boîtes mail à la réception si celles-ci sont chez Google.
- L'activité a un mail d'envoi géré par Gandi : envoi par serveurs qui est géré par Gandi, OK
- L'activité a un mail d'envoi géré par Google / OVH / Ecomail etc. Donc envoi par serveurs qui est géré par Gandi : NOK si à la réception la personne utilise Google.

*La solution future : améliorer l'envoi de mail sur Odoo pour que chaque activité puisse envoyer avec les informations de sa vraie boîte mail.*

#### 2) Les mails envoyés par les `serveurs -arobase- grap.coop` ne sont pas automatiquement enregistrés dans le dossier `/Envoyés`

- À priori, l'envoi de mail n'est pas totalement bien développé et il manque quelques informations dans le mail pour que celui-ci soit bien mis dans le dossier `/Envoyés`.
- Mais avec Google, cela fonctionnait. Il devrait réussir à comprendre qu'un mail partait de sa boite mail, et il le plaçait le mail dans le dossier `/Envoyés`. Ce qui était pratique pour vérifier que le mail était bien parti.

*La solution future : améliorer l'envoi de mail sur Odoo pour que le mail arrive dans le dossier* `/Envoyés`.

### Un comportement pas anticipé : Google, le mort-vivant

Malgré la déconnexion technique du nom de domaine grap.coop avec Google, il était encore possible de se connecter à Gmail et d'envoyer des mails. Alors certes, les réponses n'arrivaient plus sur Gmail, mais cela permettait encore aux irréductibles de résister au changement ! Surtout, même après avoir supprimé le compte Google sur Thunderbird (n'ayant alors que le compte Gandi), un paramétrage technique (le serveur SMTP d'envoi) faisait que les mails envoyés l'étaient par le serveur Google. Donc au moment de la suppression réelle du compte Google, l'envoi par Thunderbird était bloqué. Ce n'est pas un gros souci, mais nous avons documenté le [petit changement](https://librairie.grap.coop/books/mail/page/google-debranche-la-suite#bkmrk-%C2%A0-0)
à faire.

## Septembre 2022 : la fin de la route est longue, mais la voie est libre

Après la dégooglisation technique, place à la dernière étape, supprimer réellement les comptes Google. Chaque personne devait suivre un tutoriel nommé « Google débranché - La suite » sur [librairie.grap.coop/books/mail/page/google-debranche-la-suite](https://librairie.grap.coop/books/mail/page/google-debranche-la-suite) comportant ces étapes :

- Nettoyer derrière soi
- Fermer la porte
- S'assurer que l'on envoie ses mails avec les bons paramétrages
- Embellir son nouveau jardin
- Découvrir le webmail (logiciel en ligne) de Gandi
- Connecter son ordiphone
- *Quitter définitivement Google*

Il a fallu 2 mois pour que les 30 personnes concernées suivent réellement ce tutoriel - voire rattrapent leur « retard » pour sortir leur mail de Google. *Ce fut l'une des étapes les plus chronophages en termes de relance, de suivi personnel, de questions / réponses*, de gestion de cas particuliers (certaines personnes n'avaient pas pu transférer leur mail à cause d'une connexion Internet trop faible par exemple). C'est aussi à ce moment que l'on devait bien vérifier qu'aucune autre donnée n'était encore stockée sur Google Drive /
Google Photos / Agenda etc., ce qui a ralenti quelques personnes.

*Conseil num. 4 : pour motiver chaque personne à passer le pas, communiquer de façon informelle et encourageante !*

## Octobre 2022 : au revoir Google, tu ne vas pas me manquer

Même si nous avons tout fait pour être coercitifs, certaines personnes ont besoin de date limite pour prioriser leur travail. Trois semaines avant, la date butoir du 07 octobre est donc fixée pour motiver les dernières personnes.

## Novembre 2022 : jusqu'au bout !

La première date butoir et les nombreuses relances n'ont pas suffi à faire remonter en priorité n°1 à tou·te·s les collègues de sortir de Gmail.

Comme nous ne sommes pas des grands méchants, et que nous comprenons les difficultés et calendrier de chacun·e, nous redonnons du rab : *le mardi 23 novembre. La veille de la fête des 10 ans de Grap*, cela semble une date symbolique et assez lointaine pour réellement partir. Pour de bon.

*Le mardi 23 novembre*, à 13h35, nous étions 5 à nous réunir autour d'un ordinateur, observant ce moment... un peu stressant, comme quand on part d'un lieu en espérant n'y avoir rien oublié. *À 13h43, Google était derrière nous*.

![Résiliation](../img/Abonnement-resilie.png)


# La dégooglisation du GRAP, partie 3


Après la sortie de Google Drive remplacé par Nextcloud, Google Agenda par Nextcloud Agenda, nous avons fini par le plus gros bout en 2021-2022, sortir de Gmail et en finir avec le tentaculaire Google. Le mardi 23 novembre, nous débranchions enfin Google. Nous voilà libres ! Presque...

## Bilan dégooglisation


Après 4 ans de dégooglisation, où en sommes-nous de notre utilisation de
logiciels non libres ?

### Dans l'équipage


| Système d'exploitation | Libre ? | Commentaire |
|------------------------|---------|-------------|
| Windows                | non     | 13 personnes |
| Ubuntu                 | oui     | 9 personnes |
| Nextcloud Files        | oui     | Tout le monde depuis 2020 |
| Nextcloud Agenda       | oui     | Tout le monde depuis 2021 |
| **Téléphonie et visio** |        |                           |
|  3CX                   | non     | Tout le monde             |
| Nextcloud Discussions  | oui     |                           |
|  **Mail et nom de domaine** |    |                           |
| Gandi                  | oui     | Tout le monde depuis 2022 |
| **Logiciels métier**   |         |                           |
| Odoo (suivi des actis, achat/revente, facturation) | oui   | Pôles info, accompagnement et logistique |
| EBP (compta)            | non    | Pôle compta               |
| Cegid (paie)            | non    | Pôle social               |
| Gimp, Inkscape, Scribus (graphisme et mise en page) | oui   | Pôle communication  |
| BookstackApp (documentation) | oui    | Tous pôles            |
| **Logiciels bureautique** |           |                       |
| Suite Office             | non        |                       |
| Suite LibreOffice        | oui        |                       |
| **Réseaux sociaux**      |            |                       |
|  Facebook, Linkedin, Twitter, Eventbrite | non |               |
|  Peertube                | oui  |                             |



*Nos pistes d'amélioration en logiciel libre sont donc du côté du système d'exploitation et des logiciels métiers*. Les blocages sont
dus :

1. à certains logiciels métiers qui n'existent pas en logiciel libre : à voir si on arrive à développer certains bouts métier sur Odoo dans les prochaines années
2. à la difficulté de se passer d'Excel pour certaines personnes grandement habituées à ses logiques et son efficacité : à voir si LibreOffice continue à s'améliorer et/ou si on se forme plus sur LibreOffice

### Dans la coopérative

| Système d'exploitation | Libre ? | Commentaire |
|------------------------|---------|-------------|
| Ubuntu                 |   oui   | Dans tous les points de vente ordinateurs portables |
| Windows ou Mac         | non     | Les autres ordinateurs portables |
| **Gestion documentaire et travail collaboratif** |      |             |                          
| Nextcloud Files         | oui     | Tout le monde y a accès depuis 2020 |
| **Fournisseur mail principal**  |         |                            |
| Gandi                   | oui   | 55%                                 |
| Gmail                   | non   | 37%                                  |
| Ecomail                 | ?     | 4%                                    |
| **Logiciels métier**    |       |                                       |
| Odoo (achat, revente, stock, facturation, intelligence décisionnelle) | oui | Utilisé par 95% des activités    |
| Autres                  | oui et non | Dur à dire, mais la majorité des activités de transformation utilise des tableaux Excel ou des logiciels dédiés |
| **Logiciels bureautique** |        |                                      |
| Suite Office              | non    | Pas de référencement fait. Aucune visibilité actuellement |
| Suite LibreOffice         | oui    |                                     |


*Nos pistes d'amélioration sont donc du côté des logiciels mails et des logiciels métiers*. Un des gros chantiers de 2022-2023 est justement le développement et la migration sur Odoo Transfo. Pas pour le côté politique du logiciel libre mais bien de l'amélioration continue d'un même logiciel partagé dans la coopérative. À voir si la
dégooglisation de l'équipe « inspire » certaines activités pour se motiver à se dégoogliser. Nous serons là pour les accompagner et continuer à porter le message à qui veut l'entendre.

## Bilan humain


À l'heure où nous écrivons (fin octobre 2022), il est trop tôt pour faire le bilan de *la sortie de Gmail*. Nous comptons d'ailleurs envoyer un nouveau questionnaire dans quelques mois qui nous permettra d'y voir plus clair. Mais nous pouvons d'ores et déjà dire que ce *fut clairement l'étape la plus compliquée de la dégooglisation*. Sortir
d'un logiciel fonctionnel, performant et joli est forcément compliqué quand on migre vers un logiciel aux logiques différentes (logiciel bureau VS web par exemple) et qui souffre de la comparaison au premier abord. Pour compenser cela, nous avons fait le choix de *dédier beaucoup de temps humain* (nombreuses formations par mini groupes ou en individuels, réponses rapides aux questions posées) et beaucoup de documentations et de partage de retour d'expériences.

*La sortie de Google Drive et Google Agenda furent relativement douces et moins complexes que Gmail*. Le logiciel Nextcloud étant assez mature pour assurer un changement plutôt simple et serein. Ça paraît simple une fois énoncé, mais plus les gens travaillent avec un outil (Google par exemple), plus il sera difficile de les amener à changer facilement d'outil.

*Conseil num. 5 : Dans la mesure du possible, la meilleure des dégooglisation est celle qui commence dès le début, par l'utilisation d'outils Libres. En 2022, quasiment tout logiciel a son alternative Libre mature et fonctionnel. Si ce n'est pas possible, dès que les moyens humains sont disponibles et que la majorité le veut, envisagez votre dégooglisation !*

À Grap, *il existe une certaine culture politique de compréhension autour des enjeux du logiciel libre et des GAFAM*. Cela nous a aidé. Et cela nous parait quasiment obligatoire avant d'envisager une dégooglisation. Car c'est un processus long où l'on a besoin du consentement - au moins théorique - des gens impactés pour que celleux-ci acceptent de se former à de nouveaux outils, s'habituer à de nouvelles habitudes etc.

*Conseil num. 6 : avant d'entamer une dégooglisation, faire monter en compétences votre groupe sur les sujets autour du Logiciel Libre et des enjeux des Gafam à travers des projections de films par exemple*. Voici un récap.
de quelques ressources : [librairie.grap.coop/books/degooglisation/page/sinformer-quelques-liens-utiles](https://librairie.grap.coop/books/degooglisation/page/sinformer-quelques-liens-utiles).


## Bilan technique

Voici nos choix de logiciels pour notre dégooglisation :

- *Nextcloud* pour la gestion documentaire et le travail collaboratif (agenda, visio, gestion de tâches)
    - complété par *Onlyoffice* avec une image Docker sans limitation d'usage (pendant 2 ans l'image `nemskiller007/officeunleashed` puis désormais `alehoho/oo-ce-docker-license`)
    -   sauvegarde quotidienne par le logiciel de sauvegarde *Borg*
- *BookstackApp* et *Peertube* pour la documentation écrite et vidéo
- *Meshcentral* pour la prise en main à distance d'autres ordinateurs
- *Gandi* pour le prestataire de mails
- *Thunderbird* pour le logiciel bureau pour gérer ses mails (et K9Mail sur téléphone)

Voici nos choix d'infrastructure :

- OVH et Online  pour la location de serveurs faisant tourner ses services (choix historique)
- 4 serveurs :
    - 1 serveur dédié Nextcloud de 2To (Gamme Start-1-L Intel Xeon E3 1220v2 \@3,1 Ghz, 16Go RAM)
    - 1 serveur dédié Nextcloud Test en miroir du Nextcloud
    - 1 serveur de sauvegarde (mutualisé avec d'autres services de la coopérative)
    - 1 serveur dédié à différents services (Peertube, Meshcentral, Bookstackapp)

## Bilan économique

Pour calculer le coût économique de notre dégooglisation commencé en 2018, voici les chiffres retenus.

Le *scénario Dégooglisation* est celui réellement effectué depuis 2018. Son coût comprend :

- le temps de travail du service informatique, découpé en
    - aide au collègue habituelle : qui subit une augmentation du fait de l'internalisation de certaines questions, notamment avec le changement de Gmail à Thunderbird
    - support et administration système des services :
        - toutes les recherches techniques (comment bien gérer les installations, sauvegardes etc.)
        - toutes les questions / réponses par mail et téléphone
    - le « temps de dégooglisation » qui correspond
        - aux temps d'écriture de documentation et de formation
        - aux mails d'annonce, de relance, de re-re-relance
- le coût des serveurs informatiques pour faire tourner les logiciels remplaçant les services Google et Teamviewer

À l'opposé, *le scénario Google* comprend :

- le temps de travail du service informatique sur l'aide au collègue - accès stable dans le temps - qui augmente par le nombre de gens dans l'équipe, mais diminue par notre appropriation des logiciels, améliorations de l'existant, documentation etc.
- la facturation des comptes Google Workspace
    - stable depuis 2018, Google a annoncé cet été l'augmentation de ces prix. ([Les pauvres n'ont eu que 6%](https://www.sudouest.fr/economie/economie-avec-un-chiffre-d-affaires-en-hausse-de-6-seulement-la-croissance-de-google-ralentit-drastiquement-12746964.php) de croissance en 2022 avec 14 milliards de dollars de bénéfices. Passant donc les comptes pro de 4€ à 10,40€/mois à partir de juin 2023.
- la facturation hypothétique (car elle n'a jamais eu lieu) de Teamviewer Pro
    - En effet, jusqu'à juin 2019, nous utilisions Teamviewer pour  aider les activités de la coopérative à distance. Mais notre utilisation intensive ne rentrait plus dans la version gratuite et Teamviewer nous bloquait l'usage du logiciel pour que l'on souscrive à leur abonnement.
    - Heureusement, nous sommes passés sur des logiciels auto-hebergés et libre : RemoteHelp (un logiciel libre abandonné depuis) puis en décembre 2020 sur [Meshcentral](https://meshcentral.com).

**En prenant en compte ces données, le scénario *Dégooglisation* finit par devenir moins cher que le scénario *Google*.**

Pour le coût mensuel, cela arrive dès septembre 2022 (quasi à la fin de la sortie de Gmail donc) ! Pour le coût cumulé, cela devient rentable deux ans après, en septembre 2024 !

Ces chiffres s'expliquent par :

- le coût important *au démarrage* de la sortie de Google Drive : 128h passées sur les 5 premiers mois pour valider la solution Nextcloud
- un temps de support / administration système pour Nextcloud *qui baisse progressivement*, passant de 14h mensuels en 2019, à 9h en 2020, à 5h en 2021, à 3h en 2022
- le prix de Google qui aurait augmenté (mais on y a échappé avant, ouf !)

![](../img/Cout-degooglisation-GRAP.png)


## Bilan politique


Nous sommes fièr·es en tant que coopérative de porter concrètement nos valeurs dans le choix de nos logiciels qui sont plus que de simples outils. Ces outils sont porteurs de valeurs démocratiques très fortes.

Nous ne voulons pas continuer à engraisser Google - et autres [GAFAM](https://fr.wikipedia.org/wiki/GAFAM) - de nos données privées et professionnelles qui les revendent à des entreprises publicitaires et des états à tendance anti-démocratique (voir les [révélations Snowden](https://fr.wikipedia.org/wiki/R%C3%A9v%C3%A9lations_d%27Edward_Snowden), le [scandale Facebook-Cambridge Analytica](https://fr.wikipedia.org/wiki/Scandale_Facebook-Cambridge_Analytica)).

Cela est en contradiction avec ce que nous prônons : la coopération, de l'entraide et le lien humain. Nous avons besoin d'[outils conviviaux](https://fr.wikipedia.org/wiki/Outil_convivial), modulables et modifiables selon qui nous sommes. Nous avons besoin de pouvoir trifouiller les outils que nous utilisons, comme nous pouvons trifouiller un vélo pour y réparer le frein ou y rajouter un porte-bagages. Des outils émancipateurs en somme, qui nous empouvoire et ne rendent pas plus esclave de la matrice capitaliste.

Notre démarche n'aurait pas pu avoir lieu sans le travail et l'aide de millions de personnes qui ont construit des outils Libres, des documentations Libres, des conférences et autres vidéos Libres. Elle n'aurait pas eu lieu non plus sans l'inspiration de structures comme Framasoft ou la Quadrature du Net. Merci.

**La route est longue, la voie est libre, et sur le chemin nous y cueillerons des pommes bios et paysannes.**
