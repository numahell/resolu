---
title: Dégooglisation : le cas Webassoc
---

Aider les autres pour un monde numérique plus solidaire suppose un regard critique sur ses propres outils
{: .slogan-subtitle }


*WebAssoc est une pépite du monde associatif, qui déploie une énergie incroyable pour aider les autres associations à utiliser les outils numériques. Cela fait 2 ans que cette structure a engagé une réflexion sur l'adoption d'outils libres. Nous avons demandé à l'un de ses membres de nous parler de la dégooglisation de cette dynamique association.*

(Interview paru sur le Framablog le 29/07/2020)

— **Bonjour, peux-tu te présenter ? Qui es-tu ? Quel est ton parcours ?**

Bonjour Framasoft. Je
suis Jean-Luc, geek de longue date (j'ai débuté sur un
[ZX81](https://fr.wikipedia.org/wiki/ZX81)) sans que ce soit mon taf (je
bosse dans le tuyau). Je suis arrivé à WebAssoc il y deux ans. Comme je
suis branché sur le Libre depuis longtemps, Raphaëlle, notre présidente,
m'a demandé de développer son usage dans l'association.

— **Tu nous parles de ton association ?**

Chez [WebAssoc](https://www.webassoc.org/), nous
sommes plus de 1600 bénévoles et nous avons fait, en 2019, plus de 1&nbsp;300
actions auprès des associations qui nous sollicitent. Nous mettons en
relation des associations humanitaires, d'environnement ou de solidarité
avec des bénévoles du numérique qui les forment ou apportent des
renforts personnalisés.

— **Combien de salariées avez-vous ?**

Aucun
salarié, nous n'avons même pas de compte en banque ! Sur les 1600 qui
interviennent ponctuellement, 40 font tourner la machine (recevoir les
demandes, les analyser, les proposer, organiser les formations,
alimenter les réseaux sociaux, notre site...) et nous sommes 5 dans le
[bureau](https://www.webassoc.org/a-propos/) !

— **Où se trouvent vos membres ?**

Un peu partout, même si nous sommes concentrés sur Paris et
les métropoles puisque issus du monde numérique.

— **En termes d'organisation, y a-t-il une seule entité ? Plusieurs groupes de travail distincts ?**

Nous fonctionnons par bulles dans lesquelles nous
pouvons avoir plusieurs équipes de 4-5 personnes, C'est très informel et
cela nous correspond. Par exemple, je suis en charge de la bulle « tech
» qui comporte les équipes « front », « back », « crm » et « cloud »,
nous assurons toute la partie infrastructure et logiciels.

— **Tu dirais que les membres de l'association sont plutôt à l'aise avec le numérique ?**

Oui, tout le monde est à l'aise. En revanche, nous n'avons
pas tous un profil technique, bien au contraire.

— **Quelles sont les valeurs portées par l'association ?**

La joie de donner son savoir
et son temps sans se prendre la tête. Avec l'idée que lorsque nous
n'apporterons plus rien, nous passerons à autre chose. Mais les
événements récents ont montré qu'il reste du chemin pour que les
associations soient à l'aise dans leurs choix numériques.

— **Avant de lancer cette démarche, vous utilisiez quels outils ou services numériques ?**

GSuite (Gmail, Gdrive), Google Analytics, Slack, Trello,
Word, Excel, nos photos sur Flickr, notre site sous WordPress.

— **Comment est configuré le système d'information et les différentes applications ?**

Euh, vous pouvez répéter la question ? :) Plus sérieusement, à mon
arrivée, nous utilisions Wordpress et
[CiviCRM](https://www.civicrmfr.org/) sur un espace prêté. Le reste
était basé sur des outils propriétaires en mode
[SAAS.](https://fr.wikipedia.org/wiki/Logiciel_en_tant_que_service)


— **Qu'est-ce qui fonctionnait bien ? Qu'est-ce qui posait problème ?**

La première faiblesse était que nous utilisions la machine d'un
bénévole. Le reste de nos informations étaient chez d'autres (Google
pour nos documents, Gmail pour nos échanges avec l'externe, GAnalytics
pour notre trafic, Flickr, Doodle...). Cela marchait, mais nous étions
fragiles et nous ne maîtrisions pas nos données, pas top !

— **Vous avez entamé une démarche en interne pour migrer vers des outils numériques plus éthiques. Qu'est-ce qui est à l'origine de cette démarche ?**

Une volonté, appliquer à soi-même pour mieux expliquer.

— **D'où est venue l'idée, qu'est-ce qui vous a motivé⋅e⋅s ?**

WebAssoc avait déjà mis en
avant les solutions libres lors d'interventions. Mais il nous manquait
l'expérience et la légitimité, « faites ce que je dis, mais pas ce que
je fais » ne marche pas bien. :)

— **Quels sont les objectifs ? Les résultats attendus ?**

Notre feuille de route reprend quatre thèmes. Ils
sont totalement cloisonnés pour proposer notre expérience sur l'un sans
requérir les autres, d'où (on le verra plus loin) le choix de prendre un
hébergeur propre à chacun d'eux :

*Pour la planète, mettre en avant le réemploi*

Nous avons fait [notre journée annuelle en janvier](https://www.webassoc.org/events/waday2020/) sur un PC acheté 50 € sur le ... qui tournait sur [Emmabuntüs](https://emmabuntus.org/) (Linux Debian). Nous adorons cette distribution qui comprend tous les logiciels nécessaires à un bénévole, et qui a permis de belles actions comme [Yovotogo](https://yovotogo.fr/).

*Proposer un espace pour le bureau regroupant tous les outils*

Avec [NextCloud](https://nextcloud.com/fr_FR/) et ses applications (partage de fichiers, édition avec [Collabora](https://www.collaboraoffice.com/collabora-online/), visio-conférence à quelques-uns, tableau de tâches, gestion des mots de passe partagés et plein, plein d'autres choses).

*L'association sur le Web*

Avec un blog (Wordpress), des photos (Piwigo), un wiki tout simple (DokuWiki), un Framadate, un Wallabag... C'est un espace que nous considérons public donc pas de données sensibles.

*Avec l'œil RGPD*

Suivre notre trafic avec [Matomo](https://fr.matomo.org/), gérer nos
contacts et campagnes mails avec [CiviCRM](https://civicrm.org/), faire
des webinaires libre avec [BigBlueButton](https://bigbluebutton.org/) et
bientôt des vidéos avec [Peertube](https://fr.wikipedia.org/wiki/PeerTube) (merci Framasoft !)


— **Quel est le lien avec les valeurs de WebAssoc ?**

Le libre et son sens
des communs correspond bien à nos valeurs. Il a le mérite de scinder
hébergeur et logiciel. Ce qui est une garantie pour les associations :
changer d'abri (si d'aventure, les besoins évoluent) sans recommencer à
zéro la formation des bénévoles. C'est la liberté de choix en somme,
c'est évident quand on en parle et pourtant c'est tellement facile de
céder aux sirènes opposées.

— **Quels sont les moyens humains mobilisés sur la démarche ?**

Pas vraiment de moyens dédiés, nous sommes allés à
notre vitesse, en parallèle de nos anciens usages sans imposer la
bascule. Mais le constat qui m'a moi-même surpris, c'est que tout le
monde s'y est mis naturellement. Quelques usages restent (comme notre
tchat sur Slack en attendant un équivalent fonctionnel intégré à
NextCloud).

— **Y a-t-il une équipe dédiée au projet ? Ou plutôt une personne seule ?**

En fait c'est Raphaëlle et le bureau du début qui
m'ont poussé dans la piscine. J'ai proposé des expériences avec ça ou
ça. Plusieurs bénévoles (des vrais pros) se sont investis sur chaque
thème. Heureusement, parce que pour moi DNS voulait dire « Dîner
Nocturne Sympathique » !

— **Quelles compétences ont été nécessaires ?**

*Rien pour Emmabuntus* : y'a qu'à suivre [les
tutos](https://emmabuntus.org/tutoriels/).

*NextCloud pour le collaboratif* : juste installer et configurer des applis comme sur son
smartphone.

*PHP pour les services Web* : mais pas obligatoire de
développer en acceptant une esthétique simple.

*Adminsys pour gérer les VM (virtual machine)*. C'est le plus technique et ardu à tenir dans le
temps, mais pas grave si cela plante, nous n'y conservons pas
d'archivage.

— **Combien de temps ça vous a pris ?**

Je dirais une année,
mais ce n'est pas fini. Il reste des pistes à explorer.

— **Ça vous a coûté de l'argent ?**

Comme expliqué plus haut, rien. Mais c'est de
toute façon abordable : 60€/an pour NextCloud sur une instance dédiée et
80€/an pour nos services Web. Ce qui couvre déjà les besoins de la
plupart des associations. Pour rester dans le concept de transposer nos
expériences, il fallait que cela soit accessible. 50 + 60 + 80, à moins
de 200€, une association peut tout avoir sur un PC, basculer de GDrive à
NextCloud, avoir à côté du site des sous-domaines comme
[celui-ci](https://piw.webassoc.org/). En fait, une totale autonomie
avec des données chez des locaux. Par exemple, j'ai rencontré notre
hébergeur NextCloud à Niort, c'est sympa quand même. Bien sûr, pour de
l'envoi de mails en masse ou avoir son propre service de
visio-conférence, c'est plus conséquent. Mais cela va de pair avec la
taille de l'association et ses moyens. C'est comme le bio, il faut payer
un peu plus pour savoir d'où ça vient et sincèrement, les fraises ont un
autre goût.

— **Quelles étapes avez-vous suivi lors de cette démarche, au début, par exemple ?**

C'est tout bête, j'ai pris la liste de
[Degooglisons](https://degooglisons-internet.org/) sans avoir recours
aux « Framatrucs ». Vu [les
annonces](https://framablog.org/2019/09/24/deframasoftisons-internet/),
j'ai cherché des équivalents sur NextCloud ou autres services Web.
Ensuite, nous avons démarché des hébergeurs pour nous accueillir,
<https://yourownnet.net/> pour NextCloud, <https://www.o2switch.fr/>
pour nos services web, <https://www.alinto.com/> pour nos envois de
mails et tout récemment [https://www.gandi.net](https://www.gandi.netp/)
pour BigBlueButton. Nous les remercions tous énormément.

— **Les étapes intermédiaires ?**

Pas vraiment, l'équipe « tech » met à disposition,
chacun membre est libre d'utiliser ces nouveaux services. Et par
exemple, NextCloud avec l'édition en ligne peut s'utiliser sans rien
installer sur son PC.

— **À ce jour, on en est où ?**

Il reste des choses
à fiabiliser, par exemple la gestion des droits sur les dossiers, mais
rien de rédhibitoire.

— **Combien de temps entre la décision et le début des actions ?**

Le temps de trouver des gentils hébergeurs et des
gentils bénévoles.

— **Avant de migrer, avez-vous testé plusieurs outils au préalable ?**

Nous avons profité de la mise à disposition d'instances
pendant le confinement, nous avions déjà
[Talk/NextCloud](https://nextcloud.com/talk/) pour échanger à
quelques-uns et nous avons retenu BigBlueButton plutôt que Jitsi pour sa
tenue à la charge sur nos webinaires. Et maintenant, on peut piloter
BigBlueButton [depuis NextCloud](https://apps.nextcloud.com/apps/bbb),
c'est cool.

— **Avez-vous organisé un accompagnement des utilisateur⋅ices ?**

Certain⋅e⋅s connaissaient déjà, d'autres découvraient en connaissant
des équivalents propriétaires. Nous avons laissé venir les questions.

— **Sous quelle forme avez-vous répondu à ces questions ?**

Un wiki où nous avons commencé à mettre des tutos, nous avons aussi récupéré ceux
des [autres](https://framaligue.org/wp-content/uploads/Tutoriel_2020.pdf).
Et nous avons fait un webinaire dédié NextCloud.

— **Quels ont été les retours ?**

Globalement positifs. Nous prévoyons quelques séances de
visio-partage pour s'améliorer ensemble.

— **Quels sont les moyens de « l'équipe projet » ?**

Par thème, chaque équipe varie de 2 à 4 personnes
au gré du temps.

— **Quelles difficultés avez-vous rencontrées ?**

Parfois des détails,
avoir des étiquettes entre Thundebird et RoundCube comme sur Gmail.
Retrouver nos enregistrements sur BigBlueButton. Il y a un temps de
rodage à ne pas négliger. Il y aussi une difficulté plus importante de
formation des équipes pour les aider à basculer et à prendre en main les
nouveaux outils. C'est simplement une question d'habitude en réalité,
les fonctionnalités des outils étant proches, mais les habitudes sont
parfois lourdes à changer : c'est pourquoi un soutien du bureau est
utile sur ces enjeux, pour pousser au changement. En fait, y aller en
premier pour que les autres suivent.

— **Envisagez-vous de poursuivre cette démarche pour l'appliquer à d'autres aspects de votre association ?**

Nous souhaitons mettre nos webinaires en ligne avec
[PeerTube](https://fr.wikipedia.org/wiki/PeerTube), avoir un annuaire
des membres avec des accès
[SSO](https://fr.wikipedia.org/wiki/Authentification_unique) via
[OAuth](https://fr.wikipedia.org/wiki/OAuth), remplacer Slack en restant
dans NextCloud. Mais surtout, développer l'usage de CiviCRM que nous
exploitons à 10 % de ses capacités faute d'un écosystème suffisant en
France. L'effet de boule de neige du Libre manque, davantage
d'utilisateurs et utilisatrices en amène davantage encore.

— **Quels conseils donneriez-vous à d'autres organisations qui se lanceraient dans la même démarche ?**

-   Oser sauter le pas, via un hébergeur pour ne pas galérer au départ,
    y mettre quelques euros.
-   Basculer thème par thème en gardant l'ancienne solution quelques
    temps.
-   Accepter le moins « joli » en se recentrant sur ses besoins qui sont
    souvent largement couverts.

— **Le mot de la fin, pour donner envie de migrer vers les outils libres ?**

Nous ne sommes pas des libristes à fond, la preuve dans la liste de
nos formations qui sont encore pas mal orientées GAFAM. À WebAssoc, nous
ne vous dirons pas que le libre c'est bien, mais que c'est mieux (moins
cher, moins de dépendance, plus performant, plus respectueux de vos
données). Alors allons-y ensemble. :)

À l'inverse, si vous maîtrisez
NextCloud, ou si vous connaissez CiviCRM ou voulez le découvrir, ou
encore si vous avez des compétences OAuth, SSO..., enfin si vous savez
installer Peertube et d'autres trucs, venez nous rejoindre, l'ambiance
est SUPER sympa.

*Pour aller plus loin :*

-   [La feuille de route vers le libre de
    WebAssoc](https://www.webassoc.org/a-propos/notre-route-vers-le-libre/)
