---
title: Témoignages et retours d'expériences
---

Libérer son organisation, c'est sans doute l'un des travaux les plus introspectifs pour un collectif.
{: .slogan-subtitle }

Le projet [RESOLU] est issu avant tout de la mise en commun d'expériences vécues et des témoignages que nous avons pu récolter. Cependant,, il manquait une chose encore : des retours d'expérience livrés par celleux qui, au sein de leurs organisation, ont réalisé un mouvement de dégooglisation et adopté des logiciels libres.

Il s'agit d'aventures que nous reproduisons dans cette dernière partie. Ces retours d'expériences ne montrent pas seulement combien il est possible de se passer des logiciels privateurs. Ils en montrent aussi les difficultés, les freins rencontrés, les écueils et les erreurs à ne pas reproduire.

Libérer une organisation, en particulier une association, c'est d'abord partager une vision en commun des outils et des valeurs portées par le collectif. Il ne s'agit pas seulement de postuler l'adéquation entre la technique et les valeurs, il faut aussi adopter les bonnes pratiques. Ces dernières ne peuvent se décréter et c'est le message commun à tous les témoignages : rien ne se fait qui ne soit d'abord le fruit d'un assentiment collectif et d'une stratégie discutée *et* assumée collectivement.

Vivons-nosu dans un monde peuplé de licornes et autres petits oursons mignons ? que nenni. C'est à la fois un message d'espoir et une dure réalité pour les « dirigeants » qui voudraient, du haut de leur bienveillance, imposer l'adoption de logiciels libres dans leur organisation. Surtout si des logiciels privateurs en ont déjà noyauté les procédures. Car il faudra aussi prendre soin des membres, salariés ou bénévoles, souvent isolés devant leur clavier. Changer de pratiques en technologies numériques pour adopter des logiciels libres, c'est aussi prendre du recul et envisager l'organisation comme un ensemble cohérent peuplé d'acteur·ices solidaires.

