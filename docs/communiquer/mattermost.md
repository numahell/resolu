---
title: Communiquez facilement avec Mattermost
---

Une messagerie instantanée optimisée pour les équipes
{: .slogan-subtitle }

Mattermost est une solution de **communication en équipe**
adaptée aux petites et grandes structures.

Une organisation en équipes et une interface simple et intuitive permet
d'échanger via différents canaux publics ou privés afin de structurer
les conversations et ne rien perdre des informations importantes.

Toutes vos discussions organisées et au même endroit !
{: .slogan-contenu }

Vous pouvez créer différents canaux et en gérer les accès. Ces canaux de
discussion permettent de séparer les conversations en fonction des
thèmes et des personnes concernées, tout en restant sur le même outil.
Cela permet de rapidement contacter les personnes concernées par un
sujet, facilite les recherches dans l'historique, etc.

Pour mieux vous organiser, il est possible de regrouper les canaux et de
gérer plus finement les droits d'accès.

## Garder le contrôle sur ses données

Mattermost est un logiciel libre. Vous pouvez l'héberger vous-même ou
faire appel à un hébergeur éthique. Vos données seront alors gérées de
manière respectueuse et transparente par quelqu'un⋅e en qui vous avez
confiance.

## « Mais les courriels, c'est très bien »... 

Oui, mais ce n'est pas toujours le moyen le plus approprié pour
communiquer. Si les courriels sont très pratiques pour envoyer des
messages élaborés pour une correspondance avec de grandes latences, ils
ne sont pas du tout adaptés à des échanges courts, surtout à plusieurs.
Les délais sont plus élevés, les messages sont copiés sur différents
serveurs et consomment donc plus de ressources, et on finit souvent par
recevoir une montagne de courriels qui ne nous concernent pas.

Avec Mattermost, les conversations sont centralisées sur un seul
serveur, il n'y a donc pas de copie inutile de données. Chacun peut,
selon le niveau d'implication voulu, choisir de lire ou non ce qui ne
lui est pas adressé directement.

## Et bien d'autres fonctionnalités

En plus des fonctionnalités de conversation instantanée et structurée,
il est possible d'envoyer des fichiers, de répondre à un message pour
conserver le fil d'une discussion, ou encore de réagir rapidement à un
message avec des émoticônes. De plus, Mattermost offre la possibilité
d'ajouter des **extensions** permettant par exemple de
faire des sondages, intégrer de la visioconférence, etc.

## Une interface intuitive et complète

Accessible depuis un navigateur web, une application pour ordinateur ou
encore une application mobile, l'interface de Mattermost est grandement
personnalisable et se prend facilement en main.

La séparation des équipes, des salons et des messages personnels permet
de rapidement avoir une vue d'ensemble des différentes conversations.

## À vous de jouer !

Commencer à utiliser Mattermost pour communiquer au sein de votre
structure est facile. Il existe trois façons de faire :

-   Passer par un hébergeur éthique proposant une instance ouverte,
    comme [framateam.org](http://framateam.org/) ou
    [team.picasoft.net](http://team.picasoft.net/), ou d'autres CHATONS
    par exemple.

-   Héberger votre propre instance. Cela demande peu de ressources mais
    quelques compétences.

-   Utiliser la version Entreprise de Mattermost. Elle est payante mais
    offre plus de fonctionnalités pour des grandes structures.

## Par rapport à d'autres solutions

Facebook Messenger et WhatsApp sont parfois utilisés dans ce contexte.
Cependant, ce ne sont pas des outils de travail, mais des moyens de
communication pensés pour des discussions entre amis ou en famille. En
plus de ne pas offrir de fonctionnalités utiles au travail en
collaboration, ces outils sont souvent également utilisés dans la sphère
privée. Il devient alors plus difficile de séparer les activités
professionnelles ou associatives des activités personnelles.

Slack ressemble à Mattermost sur beaucoup de points. L'interface est
similaire et les usages très proches. Cependant, Slack ne peut pas être
hébergé par quelqu'un d'autre que l'entreprise qui le développe. Cela
centralise les ressources et les données dans les mains d'un seul acteur
qui peut changer sa politique à tout moment. De plus, la version payante
de Mattermost propose plus de possibilités que la version payante de
Slack, par exemple en matière de personnalisation, de gestion des
données et de transparence.

## Bonnes pratiques

Les conversations sur Mattermost peuvent devenir légèrement chaotiques
si on ne respecte pas certaines pratiques. Il est par exemple
intéressant d'utiliser la fonction « répondre » pour garder le fil d'une
conversation, d'utiliser un nom d'utilisateur⋅rice clair et/ou de
préciser son nom de façon à être facilement identifiable, et d'essayer
de trouver un équilibre entre beaucoup de salons trop spécifiques et
trop peu de salons génériques.

## Ils l'ont fait !

De nombreuses associations et petites entreprises ont déjà sauté le pas,
mais elles ne sont pas les seules. Le CERN, Uber, ING, Intel,
Département de la Défense des États-Unis, Mozilla, l'Université
Nationale de Singapour et bien d'autres grandes organisations
soutiennent Mattermost en utilisant la version Entreprise.
