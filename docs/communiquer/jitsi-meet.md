---
title: La visioconférence avec Jitsi Meet
---

Les réunions à distance deviennent simples et efficaces avec Jitsi Meet !
{: .slogan-subtitle }

Jitsi Meet est un outil de visio/audio-conférence. Il permet de faire
des réunions à distance, simplement en cliquant sur un lien.

## Comment ça marche ?

Sur la plupart des outils habituels, il faut se créer un compte en
donnant son adresse courriel ou son numéro de téléphone pour utiliser le
service. Avec Jitsi Meet, rien de tout cela. À partir de n'importe quel
ordinateur, et même uniquement par téléphone, vous pouvez rejoindre une
conférence. Vous pouvez aussi en créer une en lui donnant un nom et y
convier vos collaborateur⋅rice⋅s.

Si le lien est ouvert depuis un smartphone, il vous sera proposé de
télécharger l'application. L'application mobile est plus optimisée que
la version en navigateur, mais il est également possible d'utiliser
Jisti sur son téléphone en navigateur, sans rien installer.

Jitsi Meet est un logiciel libre utilisant une technologie assez
récente : le **WebRTC**. Les flux ne passent pas par un
serveur central, comme c'est le cas de Skype ou d'Adobe-connect qui
obligent les utilisateurs à créer des comptes et enregistrent les
messages. Jitsi permet au contraire la connexion directe entre les
ordinateurs des membres de la visioconférence, de manière
**chiffrée** et sans les enregistrer.

## Pensé pour travailler

Jitsi propose des fonctionnalités très utiles en réunion : la
possibilité de « lever la main » pour demander la parole, un espace de
clavardage, le partage d'écran pour montrer des documents ou faire des
démos\...

Grâce à tous ces outils, vos réunions peuvent se dérouler de façon calme
et productive.

## S'intègre à d'autres outils

La simplicité d'utilisation de Jitsi Meet lui permet de l'intégrer sans
problème avec d'autres outils de collaboration. Par exemple, des
extensions pour [Mattermost](mattermost.md) ou [Nextcloud](../collaborer/nextcloud.md) permettent de créer une
visioconférence directement, sans avoir à changer d'outil.

## Rien à installer

Même pas besoin de créer de compte ! Le groupe **partage un
lien** (une URL) qui ouvre directement la
visioconférence. On peut également ajouter un mot de passe pour
s'assurer que seules les personnes invitées soient présentes.

## À vous de jouer !

Jitsi Meet est un logiciel libre. Il peut être hébergé par n'importe
qui. Il existe de nombreuses instances publiques que vous pouvez
librement utiliser, en voici quelques-unes :

-   [meet.jit.si](http://meet.jit.si/)

-   [framatalk.org](http://framatalk.org/)

-   [talk.snopyta.org](http://talk.snopyta.org/)

-   [calls.disroot.org](http://calls.disroot.org/)

Et pour les universitaires et étudiant·e·s, une instance est mise à
disposition par RENATER sur
[rendezvous.renater.fr](http://rendezvous.renater.fr/).

Avant -- Je convoque une réunion, certain⋅e⋅s membres sont en
déplacement, d'autres s'occupent de leurs enfants, etc. Nous sommes deux
à la réunion, les autres membres ne se sentent pas concerné⋅e⋅s, mon
projet n'avance pas.  
Maintenant -- Je convoque une réunion en ligne, tou⋅te⋅s les membres qui
le peuvent se connectent, où qu'il⋅elle⋅s soient. Un compte-rendu est
rédigé collaborativement sur un pad lors de la réunion. La réunion se
déroule bien grâce aux outils de Jitsi et les participant·e·s se sentent
impliqué·e·s, mon projet avance.
{: .encart }

On note également que ce logiciel est intégré à la liste des logiciels
libres préconisés par l'État français dans le cadre de la modernisation
globale de ses systèmes d'informations (S.I.).

## Bonnes pratiques

Pensez à prévoir un ordre du jour à l'avance et à le faire parvenir aux
participant⋅e⋅s.

La qualité de votre visioconférence dépend en très grande partie de
votre connexion internet. Chaque fois que c'est possible, préférez une
**connexion filaire** à une connexion wifi.

Dans un échange à deux ou trois, la webcam intégrée de votre ordi et le
micro peuvent suffire. Si vous êtes plusieurs membres présent⋅e⋅s
physiquement dans une salle, l'utilisation d'un micro multidirectionnel
externe est conseillée. Il est également préférable d'utiliser un
vidéo-projecteur ou un grand écran dans ce genre de cas : utiliser
plusieurs ordinateurs dans la même salle peut provoquer des effets
larsen peu agréables.

Pensez à prévoir quelques minutes avant la conférence pour vous assurer
que le matériel fonctionne.
