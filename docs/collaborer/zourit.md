---
title: Zourit.net (témoignage)
---

Le Libre pour l'Éducation populaire et pas que...
{: .slogan-subtitle }

Zourit (la pieuvre en créole réunionnais) n'est sans doute pas le
logiciel du siècle, mais la démarche qui a conduit à sa création
peut-être considérée comme exemplaire au regard des valeurs partagées de
l'Éducation Populaire et du Libre en général. Elle vient parfaitement
illustrer ce que nous évoquons au travers de ces pages en termes de
choix et de conduite du changement.

## Au départ, une démarche interne... 

Les CEMÉA ont entamé il y a déjà plusieurs années un long et profond
travail de réflexion et de formation au sujet de leur usage des outils
numériques. Celui-ci a conduit à l'élaboration d'un « Manifeste à propos
des systèmes d'information » nous « obligeant » à repenser nos outils et
pratiques numériques.

Ce manifeste a conduit les CEMÉA à envisager (puisque nous ne parvenions
pas à trouver notre « bonheur » dans l'offre existante) le développement
des logiciels nécessaires au fonctionnement d'une structure
associative : mails, contacts, agendas partagés, cloud, visioconférence,
gestion comptable et des adhérents... L'ensemble devait être entièrement
libre (sous licence GPL), hébergé au plus près des usager⋅ère⋅s et bien
évidemment respectueux de la vie privée.

## ... mais aussi politique et éducative

Zourit équipe désormais la quasi-totalité des associations territoriales
des CEMÉA. Chacune a fait l'objet de temps de formation dédiés afin
d'accompagner ce changement auprès de nos salarié⋅e⋅s et militant⋅e⋅s.

Nous pensions nous en tenir là, en informant de la disponibilité des
sources, libre à chacun de s'en saisir ! De nombreuses associations nous
ont sollicité, non pour s'installer leur propre serveur mais pour
bénéficier d'un hébergement.

C'est finalement ce que nous avons récemment fait en devenant un membre
du collectif CHATONS (voir [chatons.org](http://chatons.org/)) sous le
nom de [zourit.net](http://zourit.net/) et en proposant non seulement
les services Zourit mais aussi et surtout un accompagnement des
associations à ces usages.

Le fait que cet outil ait été développé sous licence GPL, indique donc
qu'en aucun cas nous proposons aux associations ou collectivités locales
de tomber à nouveau dans une autre forme de dépendance. Dès qu'elles le
souhaiteront, celles-ci pourront tout à fait récupérer leurs données,
mais plus encore, chacun est libre d'installer et administrer à terme
son propre serveur Zourit. Nous souhaitons donner l'impulsion et
démontrer la faisabilité d'une autre gestion possible du « numérique »,
plus proche des usager⋅ère⋅s et porteuse de lien social en encourageant
la création de nombreux CHATONS Zourit.

## Mais que propose donc Zourit ?

Les logiciels ou services proposés sont par ailleurs présentés en détail
dans ces fiches ! On peut toutefois mentionner un appui important sur
les fonctionnalités de Nextcloud, la gestion de listes de diffusion, la
gestion comptable associative, le mailing (avec Zimbra), de la
visioconférence, et le tout avec un accès facile sur smartphone.

Afin de nous permettre de décider librement de notre destin collectif,
le contrôle démocratique de notre société numérisée et des usages qui
s'y déploient devient l'enjeu central de toute action d'Éducation
Populaire. À travers un projet comme celui-ci, nous sommes convaincu⋅e⋅s
de contribuer à inventer le futur numérique que nous voulons, pas celui
qu'on nous laissera !

Pour plus d'informations vous pouvez contacter : zourit@cemea.org
{: .slogan-contenu }
