---
title: Éthique
---

Pourquoi le choix du logiciel libre est-il un enjeu de société ?
L'Économie Sociale et Solidaire (ESS) peut-elle défendre des valeurs
éthiques sans se soucier de celles promues par les outils numériques
qu'elle utilise ?
{: .slogan-subtitle }

## Le choix d'un outil numérique est un enjeu éthique

Lorsque l'on s'interroge sur les valeurs qui guident nos actions et
nos comportements en société, on en vient parfois à essayer de trancher
ce qui relève du Bien et du Mal. Gardons-nous de parcourir cette
distance, qui mène aux questions de morale.

Réfléchir aux enjeux éthiques du numérique n'a pas pour but d'établir
une définition absolue des bons et des mauvais usages. Il s'agit plutôt
de pouvoir choisir les outils qui nous permettent d'agir en respectant
les valeurs qui, pour nous, améliorent la vie en société.
{: .encart }

L'utilisation des outils numériques a des impacts bien au-delà du monde
numérique. Nous les utilisons pour nous informer, communiquer, nous
organiser, c'est-à-dire pour agir avec les autres. Nous interagissons,
interprétons des informations et organisons des collectifs via de
multiples services et logiciels. Dans ce contexte, la maîtrise de nos
actions et décisions n'est possible que si l'on a le pouvoir de
contrôler les logiciels sur lesquels elles reposent.

Dès lors, quelles sont les conditions nécessaires pour garder le
contrôle de nos outils et agir selon nos valeurs ? Voyons en quoi tout
dépend de notre degré de liberté et de souveraineté vis-à-vis de nos
outils numériques.

## Les valeurs promues par les logiciels libres

Comme pour les organisations de l'ESS, l'éthique des logiciels libres
se retrouve dans la finalité du mouvement et dans sa gouvernance.

Les quatre libertés qui définissent le logiciel libre sont celles de ses
utilisateur⋅rice⋅s : vous êtes libre d'utiliser, d'étudier, de copier,
de modifier et redistribuer tous les logiciels libres du monde. À partir
de ces quatre libertés, le Libre renforce l'accès universel aux outils
numériques, le contrôle des utilisateur⋅rice⋅s sur leurs outils, la
transparence sur leur fonctionnement et leur traitement des données
personnelles, l'indépendance de chacun⋅e vis-à-vis de sociétés
éditrices de logiciels propriétaires.
{: .encart }

En plus du respect de la liberté de chacun, le Libre se caractérise par
un mode de gestion participatif et démocratique ; toute personne
souhaitant contribuer peut le faire, et s'intègre ainsi à une
communauté de développeur⋅se⋅s. Ce fonctionnement assure une gouvernance
horizontale des outils développés, qui sont gérés comme des
[communs numériques](../organiser/communs.md).

Le Libre encourage aussi le partage des connaissances, la mutualisation
des idées et l'ouverture de l'innovation, dans l'intérêt de
tou⋅te⋅s : toute idée implémentée dans un logiciel libre sera accessible
au monde entier et pourra être réutilisée pour faire évoluer d'autres
outils. Cette démarche solidaire vise à faire prospérer l'intelligence
collective dans le monde numérique pour que chacun⋅e puisse en faire
usage et y contribuer.

L'éthique des logiciels libres se fonde donc sur le respect de la
liberté de ses utilisateur⋅rice⋅s, une gouvernance démocratique,
l'émancipation vis-à-vis d'acteurs qui concentrent le contrôle
d'outils propriétaires ([confidentialité](../communiquer/confidentialite.md)),
la solidarité entre personnes utilisatrices et contributrices, et le partage des
connaissances.

Choisir un outil numérique, c'est se positionner sur un modèle de
société que l'on souhaite encourager. Est-ce celui d'un modèle
marchand fondé sur la revente de données personnelles, la fermeture de
l'innovation et la délégation du contrôle sur le fonctionnement et
l'évolution des outils numériques à un petit nombre d'acteurs très
puissants ? Ou est-ce plutôt celui d'un modèle dont les valeurs
reprennent point par point celles de l'Économie Sociale et Solidaire ?

## Les enjeux de la liberté numérique pour l'Économie Sociale et Solidaire

Les organisations de l'ESS s'attachent à l'éthique de leurs actions
et à leur utilité sociale. Les modes de gestion participatifs et les
modèles économiques utilisés traduisent la volonté de faire évoluer la
société vers davantage de justice, de cohésion sociale, de solidarité.

Si cette démarche citoyenne s'enracine dans des lieux, des événements
et des rencontres, elle s'opère grâce aux outils numériques qui nous
servent à communiquer, à nous informer, nous organiser et nous
mobiliser. Il est donc primordial que les acteur⋅ice⋅s de cette démarche
puissent garder le contrôle des outils qui leur permettent d'agir, et
encouragent un modèle qui en assure un contrôle démocratique.

En décidant d'orienter l'écosystème numérique vers davantage de
libertés pour tou⋅te⋅s ses utilisateur⋅ice⋅s, les acteur⋅ice⋅s de l'ESS
assurent à la fois leur souveraineté et leur pouvoir d'agir avec des
outils développés dans leur intérêt.

Le choix d'un outil numérique est un choix de société : vos usages
numériques sont autant de votes pour renforcer ou restreindre les
valeurs éthiques communes de l'ESS et du Libre.
