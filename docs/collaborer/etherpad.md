---
title: L'édition collaborative avec Etherpad
---

Éditez des documents et prenez des notes ensemble avec Etherpad
{: .slogan-subtitle }

Etherpad est un outil d'édition de documents en ligne. Il permet de
travailler à plusieurs sur un même texte en même temps, facilitant ainsi
la collaboration. Ce document est appelé un **pad**.

## Simple d'utilisation

Offrant les fonctionnalités basiques d'un logiciel de traitement de
texte, Etherpad est intuitif et ne chamboule pas vos habitudes.

Il suffit de créer un pad et de partager le lien à vos
collaborateur⋅rice⋅s pour commencer à travailler, il n'y a même pas
besoin de créer de compte !

## Pratique

Les participant⋅e⋅s ont chacun⋅e une couleur pour les reconnaître et
savoir qui a écrit quoi. Pas d'inquiétude, ces couleurs peuvent être
désactivées et n'apparaissent pas sur le document final.

Des systèmes de **commentaires** et de **conversation instantanée**
permettent de donner son avis et d'échanger en direct au sujet du
document.

De plus, Etherpad garde l'historique de tout ce qui a été tapé : pas de
risque de perte suite à une mauvaise manipulation !

## Des fonctionnalités à la carte

Etherpad est un logiciel libre, c'est-à-dire que chacun⋅e peut
l'installer et le mettre à disposition. Une fois installé et ouvert en
ligne pour le public, il devient une **instance** (par
exemple le service Framapad de l'association Framasoft est une instance
Etherpad). Chaque instance peut offrir des fonctionnalités différentes.

Il est facile d'ajouter des fonctionnalités à Etherpad grâce à des
extensions. Par exemple ajouter des thèmes, des options d'exportation,
des polices ou des couleurs. Certaines instances suppriment les pads
après un certain délai, d'autres encore proposent de protéger les pads
par mot de passe, etc.

Ces options doivent être activées par l'hébergeur de l'instance, il est
donc intéressant d'en tester plusieurs pour voir laquelle correspond le
mieux à vos besoins.

## Quelques cas d'utilisation

Les pads peuvent être utilisés pour répondre à beaucoup de besoins. On
peut par exemple s'en servir d'outil de prise de notes collaboratif lors
d'une réunion, d'espace pour un brain-storming, ou encore de traitement
de texte basique pour la rédaction d'un document.

## À vous de jouer !

Beaucoup de membres du collectif CHATONS proposent des instances
Etherpad. On peut par exemple citer :

-   [framapad.org](http://framapad.org/)

-   [pad.picasoft.net](http://pad.picasoft.net/)

-   [pad.colibris-outilslibres.org](http://pad.colibris-outilslibres.org/)

-   [pad.hadoly.fr](http://pad.hadoly.fr/)

-   [pad.chapril.org](http://pad.chapril.org/)

Et d'autres sur [chatons.org](http://chatons.org/) !


Avant -- Je crée un document, l'envoie à tout le monde pour avis. Je
reçois 5 versions avec des ajouts, je passe 30 minutes à tout regrouper
et faire un nouveau jet que j'envoie à tout le monde. Je reçois 2
corrections, une version avec des ajouts, et une réponse à la première
version qui a été modifiée depuis. Je passe 1 heure à tout regrouper,
etc.<br>
Maintenant -- Je crée un pad, j'envoie le lien à tout le monde, et je
peux me concentrer sur le contenu, qui évolue avec les contributions de
chacun·e !
{: .encart }

## Bonnes pratiques

Pensez à renseigner votre nom ou pseudo dans le champ prévu à cet effet
afin d'aider les autres à vous identifier.

Les pads doivent être considérés comme éphémères. N'oubliez pas
d'**exporter** au propre les documents que vous
souhaitez conserver.

Attention, en général les pads sont publics. Ils ne sont pas indexés
(ils restent donc invisibles) mais sont accessibles à quiconque ayant
l'adresse. Il est donc important de bien faire attention à ne pas y
laisser d'informations confidentielles ni d'informations personnelles
sans avoir l'accord des personnes concernées.

[![libertes-numeriques](../img/libertes_numeriques.jpg){: .illustration }](https://framabook.org/libertes-numeriques/){target=_blank}
*[Christophe Masutti](https://golb.statium.link/){target=_blank}    
[Libertés numériques](https://framabook.org/libertes-numeriques/){target=_blank} -
[[PDF ↓]](https://framabook.org/docs/dumo/dumo_couleur_licenceArtLibre_novembre2017.pdf){target=_blank}    
Guide de bonnes pratiques   
à l'usage des DuMo   
Framabook 2017  
Licence [Art Libre](http://artlibre.org/){target=_blank}*
{: .figure }
