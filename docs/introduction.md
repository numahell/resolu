---
title: Introduction
---

Quel bon usage des outils numériques ?<br>
Adoptez des logiciels libres et en adéquation avec vos valeurs
{: .slogan-subtitle }

Les outils numériques les plus triviaux ne sont pas toujours les plus
pratiques ni les plus éthiques. Quel prix sommes-nous prêt⋅e⋅s à payer
pour nous en servir ? Les conditions d'utilisation qui consistent
parfois à devoir accepter de divulguer ses données personnelles ainsi
que celles des membres de votre association sont-elles des contraintes
négligeables ? Pouvez-vous utiliser n'importe quel service, gratuit ou
non, sans en estimer l'adéquation avec les valeurs portées par votre
organisation, votre groupe ou votre association ? Et même si vous êtes
prêt·e à sauter le pas pour équiper vos dispositifs avec des logiciels
libres, vos collègues le sont-il⋅elle⋅s pour autant ? Avez-vous anticipé
les changements ?

Issue d'une collaboration entre Framasoft et les CEMÉA, cette collection
de fiches présente des pistes pour engager une réflexion sur les usages
et les libertés numériques. Regroupés sous les trois grands thèmes de la
**[collaboration](collaborer/index.md)**, la **[communication](communiquer/index.md)**
et l'**[organisation](organiser/index.md)**, quelques outils seront présentés
pour faciliter votre approche et adopter des solutions saines, basées
sur des logiciels libres.
{: .encart }

## Le Libre, ce n'est pas que des programmes

Le Libre est surtout un écosystème d'organisations militantes en faveur
d'un Internet différent, plus ouvert, plus sain, plus transparent.
Soutenir cet écosystème, c'est créer du lien social et contribuer à une
vision de la société basée sur le **partage** et
**l'échange** de savoir-faire et de connaissances. À
l'inverse, utiliser des outils propriétaires (ou privateurs) et peu
respectueux de nos intimités numériques, c'est accepter une économie où
quelques monopoles accaparent les ressources et dictent les usages.

Les logiciels libres comptent parmi les meilleurs véhicules collectifs
du partage, de la solidarité et de l'inclusion. Les organisations de
l'Économie Sociale et Solidaire, de l'Éducation Populaire, et toutes les
organisations qui œuvrent pour un monde meilleur devraient les utiliser
de préférence parce que dans leurs nombreuses activités il serait bien
dommage d'utiliser des outils qui ne respectent pas les
utilisateur⋅rice⋅s, violent nos vies intimes et imposent leurs usages et
leur vision du monde.

L'objectif de ce document multi-feuillets est de permettre à votre
organisation d'appréhender en douceur le passage de solutions numériques
fermées à des solutions numériques ouvertes. Parce qu'on ne peut pas
penser à tout. Parce que, pour convaincre, il faut des arguments.
{: .slogan-contenu }

## La confidentialité sur Internet

Lorsqu'on utilise un service en ligne, nos données sont sur l'ordinateur
de quelqu'un⋅e d'autre. Ce choix n'est pas sans conséquences. Si vous le
faites au nom de votre association pour un service fourni par Google, ce
sont tou⋅te⋅s les adhérent⋅e⋅s qui seront exposé⋅e⋅s aux pratiques
discutables de cette firme. Et même si vous pensez que ce choix ne
regarde que vous, le simple fait d'utiliser certaines fonctionnalités
(comme le partage de fichiers, le courriel ou un simple sondage) peut
impliquer que vos correspondant⋅e⋅s s'exposent à leur tour aux pratiques
non éthiques d'un tel fournisseur. En somme la confidentialité **n'est
jamais une affaire de choix individuel** !

## Libres, vraiment ?

Un logiciel libre se définit selon **les libertés**
suivantes, inscrites dans une licence qui accompagne le logiciel : vous
pouvez l'utiliser comme vous l'entendez, le distribuer à qui vous
voulez, et même étudier son code source, le modifier et diffuser vos
modifications.

Ainsi, à la différence des [communs numériques](organiser/communs.md), définis et créés par une
communauté. Vous en connaissez sûrement déjà, comme Firefox ou
LibreOffice, mais il en existe bien d'autres !

Ces outils sont évolutifs et appartiennent à tout le monde. Ils
répondent donc au mieux aux attentes des utilisateur⋅rice⋅s, vu
qu'eux⋅elles-mêmes peuvent faire partie du processus d'évolution.

Mieux encore, le Libre est aussi un **mouvement** qui n'intéresse pas
uniquement les logiciels : on peut aussi libérer les œuvres de l'esprit,
les connaissances, l'art, l'agriculture, la médecine... bref, le Libre,
c'est aussi un état d'esprit.
{: .encart }

## Des services web

Un service web est un outil accessible en ligne. Il est mis à
disposition par un hébergeur qui s'occupe de la disponibilité et de la
maintenance du service.

Certains services sont hébergés par des entreprises puissantes comme
Google, Facebook ou Microsoft. Ils reposent sur des programmes qui non
seulement ne sont pas libres, mais sont conçus pour capter une pléthore
d'informations sur les utilisateur⋅rice⋅s en échange d'une apparente
[gratuité](collaborer/gratuite.md). En revanche d'autres services sont
proposés par des plus petits hébergeurs à taille humaine qui ont fait
d'autres choix : utiliser des logiciels libres et **respecter les
utilisateur⋅rice⋅s par** [éthique](collaborer/ethique.md).

Tel est par exemple l'engagement
des CHATONS (Collectif des Hébergeurs Alternatifs Transparents Ouverts
Neutres et Solidaires --
[chatons.org](http://chatons.org/)) !
Ces structures proposent des services éthiques et accompagnent les
utilisateur⋅rice⋅s pour qu'ils ou elles **reprennent le
contrôle** sur leurs outils.

## Et c'est gratuit ?

Le logiciel libre est le plus souvent gratuit. Du moins, il est déjà
payé par les contributeur·rices qui y ont mobilisé du temps et de
l'argent pour le rendre disponible. L'hébergement par contre ne l'est
pas toujours. En effet, héberger un service en ligne demande du matériel
parfois coûteux et du temps, c'est pourquoi certains hébergeurs
demandent une rétribution ou une contribution pour le service qu'ils
proposent. **[→ Gratuité](collaborer/gratuite.md)**

## Vous aussi, contribuez et diffusez ces fiches !

Ces fiches sont sous licence CC-by-Sa, vous pouvez donc les réutiliser,
les modifier et les redistribuer à votre guise.

N'attendez plus, venez découvrir des outils qui vous faciliteront la
vie, et sans vendre votre âme aux GAFAM !
{: .slogan-contenu }
